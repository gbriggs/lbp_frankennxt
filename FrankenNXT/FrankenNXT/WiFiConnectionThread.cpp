#include <unistd.h>
#include <string>
#include <sys/time.h>
#include <netinet/in.h>
#include "string"
#include <iostream>
#include <sstream>

#include "WiFiConnectionThread.h"
#include "WiFiHandler.h"
#include "Parser.h"
#include "UtilityFn.h"
#include "CMDiwconfig.h"
#include "CMDhostapd.h"
#include "CMDarp.h"
#include "FileWpaSupplicant.h"
#include "CMDiwconfig.h"
#include "CMDiwlistScan.h"

using namespace std;




/////////////////////////////////////////////////////////////////////////////
//  WiFiConnectionThread
//  manages the main server connection thread, listens for connection requests from clients
//


WiFiConnectionThread::WiFiConnectionThread(WiFiHandler& wiFiHandler) :
_WiFiHandler(wiFiHandler)
{
}


WiFiConnectionThread::~WiFiConnectionThread()
{
	if (ThreadRunning)
	{
		Cancel();
	}
}




string FormatList(vector<string> listToFormat)
{
	string listFormatted = "";

	//  get available networks
	for (int i = 0; i < listToFormat.size(); i++)
		listFormatted += format("%s\n", listToFormat[i].c_str());
	
	listFormatted += "\n";
	
	return listFormatted;
}


//--------------------------------
//  RunFunction
//  this is the Thread base class override
//  this function gets called each cycle through the thread
void WiFiConnectionThread::RunFunction()
{
	struct sockaddr_in clientAddress;
	string readFromSocket;

	int acceptFileDescriptor = ReadStringFromSocket(&clientAddress, readFromSocket);

	if (acceptFileDescriptor < 0)
		return;

	_WiFiHandler._Logging.Log(">>>> " + readFromSocket + "\n");
	

	//  log event time
	timeval eventTime;
	gettimeofday(&eventTime, 0);

	//  log event sender
	char* addressOfSender = inet_ntoa(clientAddress.sin_addr);
	string eventSender = string(addressOfSender);

	//  parse the read string for known commands
	//  command syntax is "$TCP_SOMECOMMAND,argument1,argument2,...
	//
	Parser readParser(readFromSocket, ",");
	string command = readParser.GetNextString();

	string response = "";
	
	//  Look for recognized commands
	//
	if (command.compare("$LBP_GET_STATUS") == 0)
	{
		if (_WiFiHandler.ControlBusy())
			response = "$LBP_GET_STATUS,NAK,busy\n";
		else
			response = format("$LBP_GET_STATUS,ACK,%s\n", _WiFiHandler.RouterStatus().c_str());	
	}
	else if (command.compare("$LBP_GET_MODE") == 0)
	{
		if (_WiFiHandler.ControlBusy())
			response = "$LBP_GET_MODE,NAK,busy\n";
		else
			response = format("$LBP_GET_MODE,ACK,%s\n", _WiFiHandler.RouterMode().c_str());
	}
	else if (command.compare("$LBP_GET_APS") == 0)
	{
		//  format state string
		if (_WiFiHandler.ControlBusy())
			response = "$LBP_GET_APS,NAK,busy\n";
		else
			response = format("$LBP_GET_APS,ACK,%s\n", FormatList(_WiFiHandler.AvailableNetworks().AccessPoints()).c_str());
	}
	else if (command.compare("$LBP_SET_AP") == 0)
	{
		if (_WiFiHandler.RequestCommand(readFromSocket))
			response = "$LBP_SET_AP,ACK,Setting access point ...\n";
		else
			response = "$LBP_SET_AP,NAK,The Pi is already busy setting the mode, please try again later\n";
	}
	else if (command.compare("$LBP_SET_MODE") == 0)
	{
		if (_WiFiHandler.RequestCommand(readFromSocket))
			response = "$LBP_SET_MODE,ACK,Setting WiFi mode ...\n";
		else
			response = "$LBP_SET_MODE_NAK,The Pi is already busy setting the mode, please try again later\n";
	}
	else
	{
		//  unknown command
		response = format("$LBP_NAK,unknown command: %s\n", readFromSocket.c_str());
		write(acceptFileDescriptor, response.c_str(), response.size());
		_WiFiHandler._Logging.Log("<<<< " + response);
	}
	
	//  write response
	WriteStringToSocket(acceptFileDescriptor, response);
	
	_WiFiHandler._Logging.Log("<<<< " + response);

}