#include <sys/time.h>
#include <unistd.h>

#include "Logging.h"
#include "WheelEncoderThread.h"



using namespace std;


//  Constructor
//
WheelEncoderThread::WheelEncoderThread(Logging& logging, NavigationEngine& navEngine)
	: _Logging(logging), _NavigationEngine(navEngine)
{
	
}


//  Destructor
//
WheelEncoderThread::~WheelEncoderThread()
{
	if ( ThreadRunning )
	{
		Cancel();
	}

	
}




//  Cancel
//
void WheelEncoderThread::Cancel()
{
	ThreadRunning = false;
	

	Thread::Cancel();
}



//  RunFunction
//  the thread run function, will wait on condition
//  and wake up when there is a command to process
//
void WheelEncoderThread::RunFunction()
{
	while (ThreadRunning)
	{
		//  run at 50 HZ
		//usleep(20000);
		usleep(1000000);
	}
	

	return;
}