#pragma once

#include <condition_variable>
#include <queue>
#include <list>

#include "Thread.h"

class Logging;
class NavigationEngine;
class Motor;


//  Robot Control Thread
//  Processing queue for robot control commands
//
class RobotControlThread : public Thread
{
public:

	RobotControlThread(Logging& logging, NavigationEngine& navEngine, Motor& motor2, Motor& motor3);
	
	virtual ~RobotControlThread();

	virtual void Cancel();

	//  RunFunction
	virtual void RunFunction();
	
	void AddCommandToQueue(std::string command);

protected:
	
	std::mutex QueueMutex;
	std::queue<std::string> CommandQueue;
	
	//  condition variable is used for notification of thread to wake up
	bool Notified;
	std::condition_variable NotifyQueueCondition;
	std::mutex NotifyMutex;

	void Notify();

	//  reference to TheApp
	Logging& _Logging;
	NavigationEngine& _NavEngine;
	Motor& _Motor2;
	Motor& _Motor3;
};


