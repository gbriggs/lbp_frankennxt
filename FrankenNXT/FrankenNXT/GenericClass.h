#pragma once

#include <string>



//  A Generic Class
//
class GenericClass
{
public:

	//  Constructor
	GenericClass();
	
	//  Overloaded constructor
	GenericClass(std::string value);
	
	//  Destructor
	virtual ~GenericClass();

	//  Public member functions
	void MemberFunction();

	
protected:

	//  Member Variables
	std::string StringValue;
	
};


