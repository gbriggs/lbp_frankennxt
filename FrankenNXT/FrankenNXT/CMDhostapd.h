#ifndef _CMDHOSTAPD_H_
#define _CMDHOSTAPD_H_

#include <map>
#include "CMD.h"


// CMDhostapdCtrl
// Command "service hostapd status"
// parses out hostapd status, on or off
//
class CMDhostapdCtrl : public CMD
{
public:
	CMDhostapdCtrl();

	virtual ~CMDhostapdCtrl();

	//  override base class CMD::Parse so we can parse internet address out of system response
	virtual bool Parse();

	bool HostApdRunning() { return _HostApdRunning; }

protected:

	bool _HostApdRunning;

};


//  HostapdStation
//  
class HostapdStation
{
public:

	HostapdStation()
	{
		Address = "";
		Version = "";
		SelectedPairwiseCipher = "";
		TkipLocalMicFailures = "";
		TkopRemoteMicFailures = "";
		WpaptkState = "";
		WpaptkGroupState = "";
	}

	virtual ~HostapdStation();

	std::string Address;
	std::string Version;
	std::string SelectedPairwiseCipher;
	std::string TkipLocalMicFailures;
	std::string TkopRemoteMicFailures;
	std::string WpaptkState;
	std::string WpaptkGroupState;

};


// CMDhostapdAllsta
// Command "hostapd_cli all_sta"
// parse out devices connected to the router
//
class CMDhostapdAllsta : public CMD
{
public:
	CMDhostapdAllsta();
	CMDhostapdAllsta(CMDhostapdAllsta& rhs);

	virtual ~CMDhostapdAllsta();

	CMDhostapdAllsta& operator=(CMDhostapdAllsta& rhs);

	virtual bool Parse();

	std::vector<std::string> ConnectedStations();
	HostapdStation* ConnectedStation(std::string name);

protected:

	std::map<std::string, HostapdStation*>  _ConnectedStations;
};








#endif