#pragma once

#include "Thread.h"

class Logging;
class AppComponents;

//  Pulse thread
//  writes timestamp to terminal
//  flashes status lights

class PulseThread : public Thread
{
public:

	PulseThread(Logging& logging, AppComponents& appComponents);
	
	virtual ~PulseThread();

		
	//  override of Thread::Cancel so we can wake thread up before stopping run function
	virtual void Cancel();

	//  RunFunction
	virtual void RunFunction();

protected:

	AppComponents& _AppComponents;

	Logging& _Logging;
	
};



