#pragma once

#include <string>
#include <list>
#include <mutex>
#include <map>

#include "Thread.h"
#include "NetworkMonitorThread.h"
#include "WiFiConnectionThread.h"
#include "WiFiControlThread.h"
#include "CMDifconfig.h"
#include "UtilityFn.h"
#include "CMDiwconfig.h"
#include "CMDiwlistScan.h"
#include "CMDhostapd.h"
#include "CMDarp.h"
#include "Logging.h"




// Wi Fil Handler
// This class is the wi fi state monitor and control handler
// This class has two threads:
//    -  Main thread:  monitors the state of the wifi and automatically switches between router / wifi client mode
//    -  Socket server thread:  listens on server port and responds to GET / PUT / DELETE requests
//
class WiFiHandler
{
public:

	WiFiHandler(Logging& logging);
	virtual ~WiFiHandler();

	//  initialize app components for startup
	bool Start();

	//  shut down application components for exit
	void ShutDown();

	//  Main thread run state
	bool IsRunning() { return Running; }
	
	void	LogWiFiStatusToTerminal();

	//  Wi-Fi Router Properties
	CMDiwconfig WiFiState();
	std::string RouterMode();
	CMDhostapdAllsta WiFiConnections();
	CMDipNeighShow WiFiDevices();
	std::vector<std::string> KnownNetworks();
	CMDiwlistScan AvailableNetworks();

	std::string RouterStatus();

	bool ControlBusy() { return _WiFiControlThread.IsBusy(); }

	bool RequestCommand(std::string command);

	//  Wi-Fi Router Control functions
	bool SetMode(iwcMode mode);
	bool SetAccessPoint(std::string accessPoint, std::string wpaFile);
	
	//  Reference to logging
	Logging& _Logging;
	
protected:
	
	//  WiFi State Properties
	CMDiwconfig _WiFiState;
	CMDhostapdAllsta _WiFiConnections;
	CMDipNeighShow _WiFiDevices;
	std::vector<std::string> _KnownNetworks;
	CMDiwlistScan _AvailableNetworks;

	//  Properties access
	std::timed_mutex LockData;
	//  Control access
	std::timed_mutex LockControl;

	//  Wi-Fi Control
	bool Running;
	NetworkMonitorThread _NetworkMonitorThread;
	friend class NetworkMonitorThread;
	
	int ServerPort;
	WiFiConnectionThread _WiFiConnectionThread;
	WiFiControlThread _WiFiControlThread;
	//
	void SwitchToRouterMode();
	void SwitchToClientMode();
	
	
	
	std::string VersionString;
	
	timeval TimeOfLastConnection;

	
};
