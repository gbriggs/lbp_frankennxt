#pragma once

#include "TCPServerThread.h"
#include "ClientControlThread.h"

class Logging;
class AppComponents;

//  Client Connection Thread
//  Opens up TCPIP connection on port 48890 and listens for post / get commands from connected client devices
//
class ClientConnectionThread : public TCPServerThread
{
public:

	ClientConnectionThread(Logging& logging, AppComponents& appComponents);
	virtual ~ClientConnectionThread();

	bool StartClientConnectionThread();
	
	//  TCP server is running in the RunFunction
	virtual void RunFunction();

protected:

	ClientControlThread _ClientControlThread;
	
	AppComponents& _AppComponents;
	Logging& _Logging;
};

