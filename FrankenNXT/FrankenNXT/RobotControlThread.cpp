#include <sys/time.h>
#include <vector>
#include <algorithm>

#include "NavigationEngine.h"
#include "RobotControlThread.h"
#include "Logging.h"
#include "Parser.h"
#include "Vehicle.h"
using namespace std;




//  Constructor
//
RobotControlThread::RobotControlThread(Logging& logging, NavigationEngine& navEngine, Motor& motor2, Motor& motor3)
	: _Logging(logging), _NavEngine(navEngine), _Motor2(motor2), _Motor3(motor3)
{
	Notified = false;
}


//  Destructor
//
RobotControlThread::~RobotControlThread()
{
	if ( ThreadRunning )
	{
		Cancel();
	}

	
}


//  signal the thread to do something
//  will return false if it is busy doing a command already
void RobotControlThread::AddCommandToQueue(string command)
{
	//  add string to a list
	{
		LockMutex lockQueue(QueueMutex);
		CommandQueue.push(command);
	}


	//  notify thread we have a command
	Notify();

	
}




//  Cancel
//
void RobotControlThread::Cancel()
{
	ThreadRunning = false;
	
	Notify();

	Thread::Cancel();
}


//  Notify
//  sets the notification flag and notifies the condition variable
//
void RobotControlThread::Notify()
{
	Notified = true;
	NotifyQueueCondition.notify_one();
}


//  RunFunction
//  the thread run function, will wait on condition
//  and wake up when there is a command to process
//
void RobotControlThread::RunFunction()
{
	if (CommandQueue.size() == 0 && ThreadRunning)
	{
		//  wait for messages
		std::unique_lock<std::mutex> lockNotify(NotifyMutex);

		//  avoid spurious wakeups
		while(!Notified)
			NotifyQueueCondition.wait(lockNotify);

		//  check to see if we were woken up because of shutdown
		if(!ThreadRunning)
			return;
	}

	list<string> commands;
	//  empty the queue and put the events to send into a list
	{
		LockMutex lockQueue(QueueMutex);
		
		while (CommandQueue.size() > 0)
		{
			string nextCommand = CommandQueue.front();
			commands.push_back(nextCommand);
			CommandQueue.pop();
		}
	}
	
	//  with robot commands, toss everything except the latest command and act on it
//	string command = "";
//	if (commands.size() > 0)
//	{
//		command = *commands.end();
//	}  TODO - filter redundant commands
	
	list<string>::iterator it;
	for (it = commands.begin(); it != commands.end(); it++)
	{
		Parser readParser(*it, ",");
		string command = readParser.GetNextString();

		if (command.compare("$FN_DRIVE") == 0)
		{
			int speed = readParser.GetNextInt();
			int direction = readParser.GetNextInt();
		
			_NavEngine.DriveCommand(speed, direction);
		
			return;
		}
		else if (command.compare("$FN_MOTOR") == 0)
		{
			string name = readParser.GetNextString();
			int speed = readParser.GetNextInt();
			
			if (name.compare("LeftTrack") == 0 || name.compare("RightTrack") == 0)
			{
				_NavEngine.MotorCommand(name, speed);
			}
			else if (name.compare("2") == 0)
			{
				_Motor2.On(speed);
			}
			else if (name.compare("3") == 0)
			{
				_Motor3.On(speed);
			}
			else
			{
				_Logging.Log("Bad motor command command: " + command);
			}
			
			
		}
		else
		{
			_Logging.Log("Unrecognized command: " + command);
		}
		
	}

	Notified = false;
}