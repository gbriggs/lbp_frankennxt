#include <sys/time.h>
#include <vector>
#include <iostream>

#include "Logging.h"
#include "TheApp.h"
#include "Parser.h"

using namespace std;




//  Constructor
//
Logging::Logging()
{
	Notified = false;
	WriteToTerminal = true;
}


//  Destructor
//
Logging::~Logging()
{
	if ( ThreadRunning )
	{
		Cancel();
	}

	
}


//  signal the thread to do something
//  will return false if it is busy doing a command already
void Logging::Log(string command)
{
	//  add string to a list
	{
		LockMutex lockQueue(QueueMutex);
		CommandQueue.push(command);
	}


	if (WriteToTerminal)
	{	
		//  notify thread we have a command
		Notify();
	}
	
}


void Logging::PauseWriteToTerminal()
{
	WriteToTerminal = false;
}


void Logging::ResumeWriteToTerminal()
{
	WriteToTerminal = true;
	Notify();
}



//  Cancel
//
void Logging::Cancel()
{
	ThreadRunning = false;
	
	Notify();

	Thread::Cancel();
}


//  Notify
//  sets the notification flag and notifies the condition variable
//
void Logging::Notify()
{
	Notified = true;
	NotifyQueueCondition.notify_one();
}


//  RunFunction
//  the thread run function, will wait on condition
//  and wake up when there is a command to process
//
void Logging::RunFunction()
{
	if (CommandQueue.size() == 0 && ThreadRunning)
	{
		//  wait for messages
		std::unique_lock<std::mutex> lockNotify(NotifyMutex);

		//  avoid spurious wakeups
		while(!Notified)
			NotifyQueueCondition.wait(lockNotify);

		//  check to see if we were woken up because of shutdown
		if(!ThreadRunning)
			return;
	}

	list<string> commands;
	//  empty the queue and put the events to send into a list
	{
		LockMutex lockQueue(QueueMutex);
		
		while (CommandQueue.size() > 0)
		{
			string nextCommand = CommandQueue.front();
			CommandQueue.pop();
			commands.push_back(nextCommand);
		}
	}
	
	list<string>::iterator it;
	for (it = commands.begin(); it != commands.end(); ++it)
	{
		cout << *it;
	}

	Notified = false;
}