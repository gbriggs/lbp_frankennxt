#include <arpa/inet.h>
#include <string>
#include <sys/time.h>
#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <sstream>


#include "WiFiHandler.h"
#include "Parser.h"
#include "UtilityFn.h"
#include "FileWpaSupplicant.h"



using namespace std;



/////////////////////////////////////////////////////////////////////////////
//  TheApp
//  the grand central station of the program,
//  this class runs everything except the keyboard input thread, which is in main
//


//  Constructor
//
WiFiHandler::WiFiHandler(Logging& logging)
	: _Logging(logging)
	, _NetworkMonitorThread(*this)
	, _WiFiControlThread(*this) 
	, _WiFiConnectionThread(*this)
	
{
	VersionString = "1.0.1";
	Running = true;
}


//  Destructor
//
WiFiHandler::~WiFiHandler()
{
	ShutDown();
}



// Start the Wi-Fi handler thread
// opens up TCPIP server socket on port 48888 to listen for Wi-Fi related queries/posts
// kicks off the background loop to monitor Wi-Fi status
//
bool WiFiHandler::Start()
{
	//  open the server socket
	ServerPort = _WiFiConnectionThread.OpenServerSocket(48888, true);
	if (ServerPort < 0)
	{
		printf("Failed to open server socket!");
		return false;
	}

	//  start the TCPIP threads
	_WiFiConnectionThread.Start();
	
	//  Start the net monitor thread
	_NetworkMonitorThread.Start();

	//  Start the control threads
	_WiFiControlThread.Start();
	
	return true;
}



//  ShutDown
//  stops running threads, and cleans up memory allocated by TheApp object
//
void WiFiHandler::ShutDown()
{
	//  stop the connection thread
	if(_WiFiConnectionThread.IsRunning())
		_WiFiConnectionThread.Cancel();

	//  stop the network monitor thread
	if(_NetworkMonitorThread.IsRunning())
		_NetworkMonitorThread.Cancel();

	//  stop the control thread
	if(_WiFiControlThread.IsRunning())
		_WiFiControlThread.Cancel();
}






void	WiFiHandler::LogWiFiStatusToTerminal()
{
	_NetworkMonitorThread.LogWiFiStatusToTerminal();	
}


//  Properties
//

//  WiFi State
CMDiwconfig WiFiHandler::WiFiState()
{
	CMDiwconfig value;

	//  try to get a lock on the control function
	std::chrono::milliseconds timeout(100);
	if (LockData.try_lock_for(timeout))
	{
		value = _WiFiState;

		LockData.unlock();
	}

	return value;
}

//  Route Mode
string WiFiHandler::RouterMode()
{
	string mode = "";

	//  try to get a lock on the control function
	std::chrono::milliseconds timeout(100);
	if (LockData.try_lock_for(timeout))
	{
		switch (_WiFiState.Mode())
		{
		case iwcMaster:
			mode = "Master";
			break;

		case iwcManaged:
			mode = "Managed";
			break;

		default:
			mode = "Unknown";
		}

		LockData.unlock();
	}

	return mode;
}

//  Stations Connected
CMDhostapdAllsta WiFiHandler::WiFiConnections()
{
	CMDhostapdAllsta value;

	//  try to get a lock on the control function
	std::chrono::milliseconds timeout(100);
	if (LockData.try_lock_for(timeout))
	{
		value = _WiFiConnections;

		LockData.unlock();
	}

	return value;
}

//  Devices connected on wlan 0
CMDipNeighShow WiFiHandler::WiFiDevices()
{
	CMDipNeighShow value;

	//  try to get a lock on the control function
	std::chrono::milliseconds timeout(100);
	if (LockData.try_lock_for(timeout))
	{
		value = _WiFiDevices;

		LockData.unlock();
	}

	return value;
}

//  Known networks
vector<string> WiFiHandler::KnownNetworks()
{
	vector<string> value;

	//  try to get a lock on the control function
	std::chrono::milliseconds timeout(100);
	if (LockData.try_lock_for(timeout))
	{
		value = _KnownNetworks;

		LockData.unlock();
	}

	return value;
}

//  Available Networks
CMDiwlistScan WiFiHandler::AvailableNetworks()
{
	CMDiwlistScan value;

	//  try to get a lock on the control function
	std::chrono::milliseconds timeout(100);
	if (LockData.try_lock_for(timeout))
	{
		value = _AvailableNetworks;

		LockData.unlock();
	}

	return value;

}


//  Router Status
//  simple formatted string with WiFi mode status
//
string WiFiHandler::RouterStatus()
{
	string routerStatus = "Status lock failed !";

	//  try to get a lock on the control function
	std::chrono::milliseconds timeout(100);
	if (LockData.try_lock_for(timeout))
	{
		routerStatus = "";

		switch (_WiFiState.Mode())
		{
		case iwcMaster:
			{
				//  we are in router mode
				routerStatus += "Router mode at 192.168.150.1\n";
		
				//  see if we have any connected stations
				vector<string> connectedStations = _WiFiConnections.ConnectedStations();
				if (connectedStations.size() > 0)
				{
					routerStatus += "Connected Devices:\n";
					for (int i = 0; i < connectedStations.size(); i++)
					{
						ArpDevice* findDevice = _WiFiDevices.ConnectedDevice(connectedStations[i]);
						HostapdStation* station = _WiFiConnections.ConnectedStation(connectedStations[i]);
						if (findDevice != NULL && station != NULL)
							routerStatus += format("- %s\t\t%s\n", station->Address.c_str(), findDevice->IpAddress.c_str());
						else if (station != NULL)
							routerStatus += format("- %s\t\t%s\n", station->Address.c_str(), "! no IP address ");
						else
							routerStatus += "- !    Device with no IP address found.\n";
					}
				}
				else
				{
					routerStatus += "- No devices connected\n";
				}
			}
			break;

		case iwcManaged:
			{
				routerStatus += "WiFi Mode\n";

				//  see if we are connected to an available network
				//  we will switch back to router mode if not connected for long enough
				//  get available networks
				WiFiAccessPoint* connectedToAvailable = _AvailableNetworks.AccessPoint(_WiFiState.Essid());
				if (connectedToAvailable != NULL && connectedToAvailable->IsVisible())
				{
					routerStatus += format("[*]  Connected to:\t\t%s\n", connectedToAvailable->Essid.c_str());
				}
			}
			break;
		}

		//  get available networks
		vector<string> availableNetworks = _AvailableNetworks.VisibleAccessPoints();

		if (_KnownNetworks.size() > 0)
		{
			routerStatus += "Known Networks:\n";
			for (int i = 0; i < _KnownNetworks.size(); i++)
				routerStatus += format("-  %s\n", _KnownNetworks[i].c_str());
		}
		else
		{
			routerStatus += "- No known networks in wpa_supplicant.conf\n";
		}

		if (availableNetworks.size() > 0)
		{
			routerStatus += "Available Networks:\n";
			for (int i = 0; i < availableNetworks.size(); i++)
				routerStatus += format("-  %s\n", availableNetworks[i].c_str());
		}
		else
		{
			routerStatus += "-  No available networks\n";
		}

		
		LockData.unlock();
	}

	return routerStatus;
}




//  Request Command
//  queues a command request with the command processor
//  returns false if the command processor is already busy (one command at a time)
//
bool WiFiHandler::RequestCommand(string command)
{
	return _WiFiControlThread.ControlCommand(command);
}



//  Set Mode
//  sets the WiFi mode
//
bool WiFiHandler::SetMode(iwcMode mode)
{
	ostringstream formatString;
	formatString << "  --  Setting Router Mode: " << mode << endl;
	_Logging.Log(formatString.str());
	
	Sleep(2000);

	//  check to see what mode we are already in
	//  now switch to wifi client mode, or restart networking
	//  Get the state of the wlan0 interface
	CMDiwconfig checkWiFiState;
	checkWiFiState.Execute();

	//  Determine what to do based on wifi state
	switch(mode)
	{
	case iwcMaster:
		{
			if (checkWiFiState.Mode() == iwcMaster)
				return true;
		}
		break;

	case iwcManaged:
		{
			if (checkWiFiState.Mode() == iwcManaged)
				return true;
		}
		break;

	default:
		//  bad mode
		return false;
	}

	//  try to get a lock on the control function
	std::chrono::milliseconds timeout(2000);
	if (LockControl.try_lock_for(timeout))
	{
		switch (mode)
		{
		case iwcMaster:
			{
				//  set connected time
				gettimeofday(&_NetworkMonitorThread.TimeOfLastSwitchToRouterMode, 0);

				//  switch to wifi client mode
				CMD switchWiFi("/usr/local/sbin/switchToRouter");
				switchWiFi.Execute();
			}
			break;

		case iwcManaged:
			{
				//  set connected time
				gettimeofday(&_NetworkMonitorThread.TimeOfLastConnection, 0);

				//  switch to wifi client mode
				CMD switchWiFi("/usr/local/sbin/switchToWlanClient");
				switchWiFi.Execute();
			}
			break;
		}

		LockControl.unlock();

		return true;
	}
	else
		return false;
}


//  Set Access Point
//  sets the access point and password
//  will switch from master to managed if required
//
bool WiFiHandler::SetAccessPoint(string accessPoint, string password)
{
	ostringstream formatString;
	formatString  << "  --  Setting access point ..." << endl;
	_Logging.Log(formatString.str());
	
	//  try to get a lock on the control function
	std::chrono::milliseconds timeout(2000);
	if (LockControl.try_lock_for(timeout))
	{
		Sleep(2000);

		//  open the wpa_supplicant file
		//  get known networks from /etc/wpa_supplicant/wpa_supplicant.conf
		FileWpaSupplicant checkWpaSupplicant;
		if (!checkWpaSupplicant.ParseFile())
		{
			LockControl.unlock();
			return false;
		}

		if (!checkWpaSupplicant.UpdateFile(accessPoint, password))
		{
			LockControl.unlock();
			return false;
		}

		//  set connected time
		gettimeofday(&_NetworkMonitorThread.TimeOfLastConnection, 0);

		//  now switch to wifi client mode, or restart networking
		//  based on the current state of the wlan0 interface
		CMDiwconfig checkWiFiState;
		checkWiFiState.Execute();

		//  Determine what to do based on wifi state
		switch(checkWiFiState.Mode())
		{
		case iwcMaster:
			{
				//  switch to wifi client mode
				CMD switchWiFi("/usr/local/sbin/switchToWlanClient");
				switchWiFi.Execute();
			}
			break;

		case iwcManaged:
			{
				//  restart networking in managed mode
				//  this may be overkill, TODO - can you just restart wlan interface?
				CMD switchWiFi("/usr/local/sbin/switchToWlanClient");
				switchWiFi.Execute();
			}
			break;
		}

		LockControl.unlock();
		return true;
	}
	else
	{
		return false;
	}
}


