#include "WiFiHandler.h"
#include "FileWpaSupplicant.h"
#include <sys/time.h>
#include <sstream>

using namespace std;

//  Network Monitor Thread
//  runs in a loop to check wifi state
//  when in router mode, will check for connected clients
//  when no clients are connected, will switch to wifi mode if known networks are detected
//  
NetworkMonitorThread::NetworkMonitorThread(WiFiHandler& wiFiHandler)
	: _WiFiHandler(wiFiHandler)
{
	//  time tag
	gettimeofday(&TimeOfLastConnection, 0);
	TimeOfLastSwitchToRouterMode = (struct timeval){ 0 };
}


//  Run Function
//
void NetworkMonitorThread::RunFunction()
{
	//  update all the state variables
	CMDiwconfig wiFiState;
	wiFiState.Execute();
	
	CMDhostapdAllsta wiFiConnections;

	if (wiFiState.Mode() == iwcMaster)
		wiFiConnections.Execute();

	CMDipNeighShow wiFiDevices;
	wiFiDevices.Execute();

	//  get known networks from /etc/wpa_supplicant/wpa_supplicant.conf
	FileWpaSupplicant checkWpaSupplicant;
	checkWpaSupplicant.ParseFile();
	vector<string> knownNetworks = checkWpaSupplicant.WpaNetworks();

	CMDiwlistScan availableNetworks;
	availableNetworks.Execute();

	//  set the current state data
	//  get a lock on the data for this thread
	std::chrono::milliseconds timeout(1000);
	if (_WiFiHandler.LockData.try_lock_for(timeout))
	{
		_WiFiHandler._WiFiState = wiFiState;
		_WiFiHandler._WiFiConnections = wiFiConnections;
		_WiFiHandler._WiFiDevices = wiFiDevices;
		_WiFiHandler._KnownNetworks = knownNetworks;
		_WiFiHandler._AvailableNetworks = availableNetworks;

		_WiFiHandler.LockData.unlock();
	}
	else
	{
		//  whoops ?
		Sleep(5000);
		return;
	}

	

	//  current time
	timeval timeNow;
	gettimeofday(&timeNow, 0);

	//  Determine if we need to switch state
	switch(wiFiState.Mode())
	{
	case iwcMaster:
		{
			//  if it has been at least 2 minute since last user switch to master mode, check for auto switch back to managed mode
			if(DurationMilliseconds(TimeOfLastSwitchToRouterMode, timeNow) > 120000)
			{
				//  Check to see if there is a visible network we can connect to
				WiFiAccessPoint* nextAp = NULL;
				for (int i = 0; i < knownNetworks.size(); i++)
				{
					//  see if we have an access point, and is it visible
					nextAp = availableNetworks.AccessPoint(knownNetworks[i]);
					if (nextAp != NULL && (nextAp->IsVisible()))
						break;
					else
						nextAp = NULL;
				}

				//  we we found an available network, switch to wifi mode if we have no clients connected
				if(wiFiConnections.ConnectedStations().size() == 0 && nextAp != NULL)
				{
					if (!_WiFiHandler.RequestCommand("$LBP_SET_MODE,Managed"))
						Sleep(10000); 	//  busy processing a command, wait a while before resuming loop
				}
			}
		}
		break;

	case iwcManaged:
		{
			//  see if we are connected to an available network
			WiFiAccessPoint* connectedToAvailable = availableNetworks.AccessPoint(wiFiState.Essid());
			if (connectedToAvailable != NULL && connectedToAvailable->IsVisible())
			{
				//  we are connected still, update connected time
				gettimeofday(&TimeOfLastConnection, 0);
			}

			//  if it has been 1 minute since we were last connected, switch back to router mode
			if(DurationMilliseconds(TimeOfLastConnection, timeNow) > 60000)
			{
				if (!_WiFiHandler.RequestCommand("$LBP_SET_MODE,Master"))
					Sleep(10000); 	//  busy processing a command, wait a while before resuming loop
			}

		}
		break;

	default:
		break;
	}

	Sleep(5000);

	return;
}


void NetworkMonitorThread::LogWiFiStatusToTerminal()
{
	//  trace out the current status to the screen
	CMD getDate("date");
	getDate.Execute();
	//
	ostringstream formatString;
	formatString  << "//****  frankenNXT Wi-Fi Monitor: " << getDate.GetCommandResponse() << endl;
	_WiFiHandler._Logging.Log(formatString.str());
	formatString.str("");
	
	formatString << "//****  Server port: " << _WiFiHandler.ServerPort << endl;
	_WiFiHandler._Logging.Log(formatString.str());
	formatString.str(""); 
	
	formatString << _WiFiHandler.RouterStatus() << endl;
	_WiFiHandler._Logging.Log(formatString.str());
	formatString.str("");
	
	formatString << "****//" << endl;
	_WiFiHandler._Logging.Log(formatString.str());

}