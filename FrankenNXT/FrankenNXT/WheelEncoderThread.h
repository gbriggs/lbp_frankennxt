#pragma once

#include "Thread.h"


class Logging;
class NavigationEngine;

//  GPS Class
//  Initializes GPS module and queries positions in a therad

class WheelEncoderThread : public Thread
{
public:

	WheelEncoderThread(Logging& logging, NavigationEngine& navEngine);
	
	virtual ~WheelEncoderThread();

		
	//  override of Thread::Cancel so we can wake thread up before stopping run function
	virtual void Cancel();

	//  RunFunction
	virtual void RunFunction();

protected:

	Logging& _Logging;
	NavigationEngine& _NavigationEngine;

	
};



