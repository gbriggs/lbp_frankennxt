#pragma once

#include "Thread.h"


//  Generic Thread Implementation

class GenericThreadImplementation : public Thread
{
public:

	GenericThreadImplementation();
	
	virtual ~GenericThreadImplementation();

		
	//  override of Thread::Cancel so we can wake thread up before stopping run function
	virtual void Cancel();

	//  RunFunction
	virtual void RunFunction();

protected:

	

	
};



