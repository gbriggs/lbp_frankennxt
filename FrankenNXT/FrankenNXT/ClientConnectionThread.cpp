#include <unistd.h>
#include <string>
#include <sys/time.h>
#include <netinet/in.h>
#include <string>
#include <iostream>
#include <sstream>

#include "ClientConnectionThread.h"
#include "Logging.h"
#include "Parser.h"
#include "UtilityFn.h"
#include "CMDiwconfig.h"
#include "CMDhostapd.h"
#include "CMDarp.h"
#include "FileWpaSupplicant.h"
#include "CMDiwconfig.h"
#include "CMDiwlistScan.h"
#include <WiringPiExtension.h>
#include "AppComponents.h"

using namespace std;




/////////////////////////////////////////////////////////////////////////////
//  ClientConnectionThread


ClientConnectionThread::ClientConnectionThread(Logging& logging, AppComponents& appComponents)
	: _ClientControlThread(logging), _Logging(logging), _AppComponents(appComponents)
{
}


ClientConnectionThread::~ClientConnectionThread()
{
	if (ThreadRunning)
	{
		Cancel();
	}
}


bool ClientConnectionThread::StartClientConnectionThread()
{
	//  query / post commands - runs at lower priority
	int port = OpenServerSocket(48890, true);
	if (port < 0)
	{
		_Logging.Log("Failed to open server socket for client connection on port 48890!");
		return false;
	}
	
	//  start the processing queue
	_ClientControlThread.Start();
	
	//  start this TCPIP listener thread
	Start();
	
	return true;
	
}




//  RunFunction
//
void ClientConnectionThread::RunFunction()
{
	int numberOfLights = 0;
	struct sockaddr_in clientAddress;
	string readFromSocket;

	int acceptFileDescriptor = ReadStringFromSocket(&clientAddress, readFromSocket);

	if (acceptFileDescriptor < 0)
		return;
	
	_Logging.Log(">>>> " + readFromSocket + "\n");

	//  parse the read string for known commands
	//  command syntax is "$FN_GETSOMETHING,argument1,argument2,...
	//    or ""$HB_POSTSOMETING,argument1,argument2,...
	//
	Parser readParser(readFromSocket, ",");
	string command = readParser.GetNextString();

	//  Look for recognized commands
	//
	if (command.compare("$FN_CONNECT") == 0)
	{
		string response = "$FN_CONNECT,ACK\n";
		WriteStringToSocket(acceptFileDescriptor, response);
		_Logging.Log("<<<< " + response); 
	}
	else
	{
		//  unrecognized command
		ostringstream formatString;
		formatString << command << ",NAK" << endl;
		string response = formatString.str();
		WriteStringToSocket(acceptFileDescriptor, response);
		_Logging.Log("<<<< " + response);
	}
	


}