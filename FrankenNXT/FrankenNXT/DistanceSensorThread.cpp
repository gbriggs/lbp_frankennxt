
#include "Logging.h"
#include "DistanceSensorThread.h"
#include "NavigationEngine.h"
#include "libvl53l0x.h"
#include <WiringPiExtension.h>
#include <unistd.h>

using namespace std;


//  Constructor
//
DistanceSensorThread::DistanceSensorThread(Logging& logging, NavigationEngine& navEngine)
	: _Logging(logging)
	, _NavigationEngine(navEngine)
	
{
	
}


//  Destructor
//
DistanceSensorThread::~DistanceSensorThread()
{
	if ( ThreadRunning )
	{
		Cancel();
	}

	
}



//  Start
//
void DistanceSensorThread::Start()
{
	SensorIndex = vl53l0xStartDevice();

	Thread::Start();
}

//  Cancel
//
void DistanceSensorThread::Cancel()
{
	ThreadRunning = false;
	
	Thread::Cancel();
}



//  RunFunction
//  the thread run function, will wait on condition
//  and wake up when there is a command to process
//
void DistanceSensorThread::RunFunction()
{
	int j = 0;
	while (ThreadRunning)
	{
		
		int measure = vl53l0xDeviceReading(SensorIndex);
		if (measure != -1)
			measure = measure;
		else
			measure = 0;
		
		
		_NavigationEngine.UpdateFrontDistanceSensor(measure);
		
		//  run at 50 Hz
		usleep(20000);
	
	}
	

	return;
}