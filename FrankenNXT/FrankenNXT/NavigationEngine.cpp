//  GenericClass.cpp

#include "NavigationEngine.h"
#include <iostream>

#include "Logging.h"
#include "Vehicle.h"
#include "UtilityFn.h"


using namespace std;


//  Constructor
//
NavigationEngine::NavigationEngine(Logging& logging,  Vehicle& vehicle) : _Logging(logging),  _Vehicle(vehicle)
{
	
	
}


//  Destructor
//
NavigationEngine::~NavigationEngine()
{
	
}




bool NavigationEngine::UpdateFrontDistanceSensor(int measure)
{
	FrontDistanceSensorMeasurement = measure;
}


int NavigationEngine::GetLeftTrackSpeed()
{
	return _Vehicle.GetLeftTrackSpeed();
}


int NavigationEngine::GetRightTrackSpeed()
{
	return _Vehicle.GetRightTrackSpeed();
}


void NavigationEngine::DriveCommand(int speed, int direction)
{
	_Vehicle.Drive(speed, direction);
	
}
	

void NavigationEngine::MotorCommand(string name, int speed)
{
	if (name.compare("1") == 0 || name.compare("LeftTrack") == 0)
	{
		_Vehicle.LeftTrackOn(speed);
	}
	else if (name.compare("4") == 0 || name.compare("RightTrack") == 0)
	{
		_Vehicle.RightTrackOn(speed);
	}
	
}
