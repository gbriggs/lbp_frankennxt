#pragma once

//  Motor Class
//
class Motor
{
public:

	Motor();
	
	virtual ~Motor();
	
	void SetupMotor(int pwm, int dir1, int dir2);
	
	void On(int speed);
	
	int GetSpeed() 
	{
		return Speed;
	}

protected:
	//  Pin Numbers for this motor
	int Pwm; // 0 to 100  positive integer 
	int Direction1;  // forward:  Direction1=1 Direction2=0
	int Direction2; // forward:  Direction1=0 Direction2=1
	
	int Speed;
	
};



//  Vehicle Class
//
class Vehicle
{
public:

	Vehicle(Motor& leftMotor, Motor& rightMotor);
		
	virtual ~Vehicle();

	void Drive(int speed, int direction);
	
	void LeftTrackOn(int speed);
	
	void RightTrackOn(int speed);
	
	int GetLeftTrackSpeed()
	{
		return LeftMotor.GetSpeed();
	}
	
	int GetRightTrackSpeed()
	{
		return RightMotor.GetSpeed();
	}
	
protected:
	
	Motor& LeftMotor;
	Motor& RightMotor;	

};


