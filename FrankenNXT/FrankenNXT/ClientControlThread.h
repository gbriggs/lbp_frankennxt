#pragma once

#include <condition_variable>
#include <queue>
#include <list>

#include "Thread.h"

class Logging;


//  Client Control Thread
//  Processing queue for client post status events
//
class ClientControlThread : public Thread
{
public:

	ClientControlThread(Logging& logging);
	
	virtual ~ClientControlThread();

	virtual void Cancel();

	virtual void RunFunction();
	
	void AddCommandToQueue(std::string command);

protected:
	
	std::mutex QueueMutex;
	std::queue<std::string> CommandQueue;
	
	bool Notified;
	std::condition_variable NotifyQueueCondition;
	std::mutex NotifyMutex;

	void Notify();

	//  reference to TheApp
	Logging& _Logging;
};


