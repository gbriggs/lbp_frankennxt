#include <arpa/inet.h>
#include <string>
#include <sys/time.h>
#include <algorithm>
#include <iostream>
#include <map>
#include <WiringPiExtension.h>
#include <string>
#include <sstream>


#include "TheApp.h"
#include "Parser.h"
#include "UtilityFn.h"
#include "FileWpaSupplicant.h"



using namespace std;



/////////////////////////////////////////////////////////////////////////////
//  TheApp
//  the grand central station of the program,
//  this class coordinates activity between different program objects


//  Constructor
//
TheApp::TheApp(Logging& logging) :
	 _Logging(logging)
	, _PulseThread(logging, _AppComponents)
	, _WiFiHandler(logging)
	, _Vehicle(_Motor1, _Motor4)
	, _NavigationEngine(logging, _Vehicle)
	, _RCConnectionThread(logging, _NavigationEngine, _Motor2, _Motor3)
	, _ClientConnectionThread(logging, _AppComponents)
	, _DistanceSensorThread(logging, _NavigationEngine)
	, _ImuThread(logging, _NavigationEngine)
	, _WheelEncoderThread(logging, _NavigationEngine)
	,  _AppComponents(_WiFiHandler, _NavigationEngine, _Vehicle, _Motor2, _Motor3)
	
{
	VersionString = "1.0.1";
	
}


//  Destructor
//
TheApp::~TheApp()
{
	ShutDown();
}



//  InitializeInstance
//  perform all the required steps for the application to start
//
bool TheApp::Start()
{
	//  logging thread
	_Logging.Start();
	
	//  wi-fi handler - listens for TCPIP on port 48888
	if (!_WiFiHandler.Start())
	{
		_Logging.Log("Failed to start Wi-Fi Handler");
		return false;
	}
	
	//  motors
	_Motor1.SetupMotor(108, 109, 110);		//  left track
	_Motor4.SetupMotor(107, 106, 105);		//  right track
	
	_Motor2.SetupMotor(113, 111, 112);
	_Motor3.SetupMotor(102, 104, 103);
	
	//  driving commands - listens for TCPIP on port 48889
	if  ( ! _RCConnectionThread.StartRCConnectionThread())
	{
		_Logging.Log("Failed to start robot connection thread");
		return false;
	}
	
	//  client connections - listens for TCPIP on port 48890
	if (!_ClientConnectionThread.StartClientConnectionThread())
	{
		_Logging.Log("Failed to start client connection thread");
		return false;
	}
	
	//  sensor threads
	_DistanceSensorThread.Start();
//	_WheelEncoderThread.Start();
//	_ImuThread.Start();
	
	//  pulse threads for periodic logging to console
	_PulseThread.Start();


	
	return true;
}



//  ShutDown
//  stops running threads, and cleans up memory allocated by TheApp object
//
void TheApp::ShutDown()
{
	//  stop the connection thread
	_WiFiHandler.ShutDown();
	
	
}

//  Is running flag
//
bool TheApp::IsRunning()
{ 
	return _WiFiHandler.IsRunning();
}


//  Pause log to terminal
//
void TheApp::PauseWriteLogsToTerminal()
{
	_Logging.PauseWriteToTerminal();
}
	

//  Resume log to terminal
//
void TheApp::ResumeWriteLogsToTerminal()
{
	_Logging.ResumeWriteToTerminal();
}

//  Write current wifi status to log
//
void TheApp::LogWiFiStatusToTerminal()
{
	_WiFiHandler.LogWiFiStatusToTerminal();
}