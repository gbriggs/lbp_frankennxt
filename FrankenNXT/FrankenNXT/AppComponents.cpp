#include "AppComponents.h"


//  Constructor
AppComponents::AppComponents( WiFiHandler& wiFiHandler, NavigationEngine& navEngine, Vehicle& vehicle, Motor& motor2, Motor& motor3) :
	 _WiFiHandler(wiFiHandler)
	, _NavigationEngine(navEngine)
	, _Vehicle(vehicle)
	, _Motor2(motor2)
	, _Motor3(motor3)
{
	
}