#include <sys/time.h>
#include <sstream>
#include "Logging.h"
#include "CMD.h"
#include "PulseThread.h"
#include "AppComponents.h"
#include "NavigationEngine.h"
#include "Vehicle.h"

using namespace std;


//  Constructor
//
PulseThread::PulseThread(Logging& logging, AppComponents& appComponents) :
	_Logging(logging), _AppComponents(appComponents)
	
{
	
}


//  Destructor
//
PulseThread::~PulseThread()
{
	if ( ThreadRunning )
	{
		Cancel();
	}

	
}




//  Cancel
//
void PulseThread::Cancel()
{
	ThreadRunning = false;
	

	Thread::Cancel();
}



//  RunFunction
//  the thread run function, will wait on condition
//  and wake up when there is a command to process
//
void PulseThread::RunFunction()
{
	while (ThreadRunning)
	{
		//  trace out the current status to the screen
		CMD getDate("date");
		getDate.Execute();
		//
		ostringstream formatString;
		formatString  << "//**    frankenNXT: " << getDate.GetCommandResponse() << endl;
		_Logging.Log(formatString.str());
		
		formatString.str("");
		formatString << "//        - Left Track: " << _AppComponents._NavigationEngine.GetLeftTrackSpeed() << endl;
		_Logging.Log(formatString.str());
		
		formatString.str("");
		formatString << "//        - Right Track: " << _AppComponents._NavigationEngine.GetLeftTrackSpeed() << endl;
		_Logging.Log(formatString.str());
		
		formatString.str("");
		formatString << "//        - Engine: " << _AppComponents._Motor2.GetSpeed() << endl;
		_Logging.Log(formatString.str());
		
		
		formatString.str("");
		
		formatString << "//        - Distance Sensor: " << _AppComponents._NavigationEngine.GetFrontDistanceSensor() << endl;
		_Logging.Log(formatString.str());
		
		//  flash the status lights here
		Sleep(1000);
	}
	

	return;
}