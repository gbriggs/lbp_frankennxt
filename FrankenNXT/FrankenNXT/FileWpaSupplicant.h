#ifndef _FILE_WPASUPPLICANT
#define _FILE_WPASUPPLICANT

#include <string>
#include <map>
#include <vector>


class WpaNetwork
{
public:
	WpaNetwork();

	std::string SsidKey();
	std::string SsidValue();
	void SsidKeyValuePair(std::string kvPair) { _SsidKeyValuePair = kvPair; }

	void PasswordKeyValuePair(std::string kvPair) { _PasswordKeyValuePair = kvPair; }

	std::string PasswordKey();
	std::string PasswordValue();
	void SetPassword(std::string password);
	

	std::vector<std::string> UnparsedKeyValuePairs;

protected:

	std::string _SsidKeyValuePair;
	std::string _PasswordKeyValuePair;

	
};


// FileWpaSupplicant
// parses the file /etc/wpa_supplicant.conf
//

class FileWpaSupplicant 
{
public:
	FileWpaSupplicant();

	virtual ~FileWpaSupplicant();

	//  parse the file
	bool ParseFile();
	bool UpdateFile(std::string accessPoint, std::string password);

	std::vector<std::string> WpaNetworks();
	WpaNetwork* Network(std::string name);
	
protected:

	std::vector<std::string> _Comments;

	std::map<std::string, WpaNetwork*> _WpaNetworks;

};



#endif