#include <unistd.h>
#include <string>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "string"
#include <iostream>
#include <sstream>

#include "RCConnectionThread.h"
#include "NavigationEngine.h"
#include "Logging.h"
#include "Parser.h"
#include "UtilityFn.h"


using namespace std;




/////////////////////////////////////////////////////////////////////////////
//  RCConnectionThread


RCConnectionThread::RCConnectionThread(Logging& logging, NavigationEngine& navEngine, Motor& motor2, Motor& motor3) :
_Logging(logging), _NavEngine(navEngine), _RobotControlThread(logging, navEngine, motor2, motor3)
{
}


RCConnectionThread::~RCConnectionThread()
{
	if (ThreadRunning)
	{
		Cancel();
	}
}


//  Start the connection thread and the command processing thread
//
bool RCConnectionThread::StartRCConnectionThread()
{
	//  robot command connection - in its own thread for priority handling
	int port = OpenServerSocket(48889, true);
	if (port < 0)
	{
		_Logging.Log("Failed to open server socket for robot connection on port 48889!");
		return false;
	}
	
	//  start the processing queue
	_RobotControlThread.Start();
	
	//  start the TCPIP server listening on this thread
	Start();
	
	return true;
}



//  RunFunction
//
void RCConnectionThread::RunFunction()
{
	struct sockaddr_in clientAddress;
	string readFromSocket;

	int acceptFileDescriptor = ReadStringFromSocket(&clientAddress, readFromSocket);
	
	if (acceptFileDescriptor < 0)
		return;
	
	_Logging.Log(">>>> " + readFromSocket + "\n");

	//  parse the read string for known commands
	//  command syntax is "$FN_SOMECOMMAND,argument1,argument2,...
	//
	Parser readParser(readFromSocket, ",");
	string command = readParser.GetNextString();
	
	//  Look for recognized commands
	//
	if (command.compare("$FN_DRIVE") == 0 || command.compare("$FN_MOTOR") == 0)
	{
		ostringstream formatString;
		formatString << command << ",ACK" << endl;
		string response = formatString.str();
		WriteStringToSocket(acceptFileDescriptor, response);
		
		_RobotControlThread.AddCommandToQueue(readFromSocket);
		_Logging.Log("<<<< " + response);
	}
	else
	{
		//  unrecognized command
		ostringstream formatString;
		formatString << command << ",NAK" << endl;
		string response = formatString.str();
		WriteStringToSocket(acceptFileDescriptor, response);
		_Logging.Log("<<<< " + response + "\n");
	}

	return;
}