#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>

#include "Logging.h"
#include "ImuThread.h"



using namespace std;


//  Constructor
//
ImuThread::ImuThread(Logging& logging, NavigationEngine& navEngine)
	: _Logging(logging), _NavigationEngine(navEngine)
{
	
}


//  Destructor
//
ImuThread::~ImuThread()
{
	if ( ThreadRunning )
	{
		Cancel();
	}

	
}




//  Cancel
//
void ImuThread::Cancel()
{
	ThreadRunning = false;
	

	Thread::Cancel();
}



//  RunFunction
//  the thread run function, will wait on condition
//  and wake up when there is a command to process
//
void ImuThread::RunFunction()
{
	while (ThreadRunning)
	{
		//  run at 50 HZ
		//usleep(20000);
		usleep(1000000);
	}
	

	return;
}