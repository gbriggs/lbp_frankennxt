#pragma once

#include <condition_variable>
#include <queue>

#include "Thread.h"


class WiFiHandler;

//  Wi-Fi Control Thread
//  Processing queue for Wi-Fi status query and control commands
//
class WiFiControlThread : public Thread
{
public:

	WiFiControlThread(WiFiHandler& wiFiHandler);
	
	virtual ~WiFiControlThread();

	//  IsBusy, true when control thread is busy processing a command
	bool IsBusy() { return _IsBusy; }
	//  signal the control thread to do something
	//  will return false if it is busy doing a command already
	bool ControlCommand(std::string command);

	//  override of Thread::Cancel so we can wake thread up before stopping run function
	virtual void Cancel();

	//  RunFunction
	virtual void RunFunction();

protected:

	// the command to execute
	std::string Command;

	//  simple mechanism to enforce only one command at a time
	bool _IsBusy;

	//  condition variable is used for notification of thread to wake up
	bool Notified;
	std::condition_variable NotifyMessagesCondition;
	std::mutex NotifyMutex;

	void Notify();

	//  reference to TheApp
	WiFiHandler& _WiFiHandler;
};


