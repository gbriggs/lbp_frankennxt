#pragma once
#include <string>

class Logging;
class ProjectionEngine;
class Vehicle;

//  Navigation Engine
//   - consolidates all inputs fron spatial sensors in one place
//   - accepts manual driving commands
//   - issues driving commands to vehicle

class NavigationEngine
{
public:

	//  Constructor
	NavigationEngine(Logging& logging, Vehicle& vehicle);
	
	//  Destructor
	virtual ~NavigationEngine();

	int GetLeftTrackSpeed();
	int GetRightTrackSpeed();
	
	//  Properties
	int GetFrontDistanceSensor()
	{
		return FrontDistanceSensorMeasurement;
	}
		
	bool UpdateFrontDistanceSensor(int merasure) ;
	
	//  Navigation engine user inputs
	void DriveCommand(int speed, int direction);
	void MotorCommand(std::string name, int speed);
	void MotorCommand(int index, int speed);
	
protected:

	double North, East, Elev;

	Logging& _Logging;
	Vehicle& _Vehicle;
	
	//  properties
	int FrontDistanceSensorMeasurement;
};


