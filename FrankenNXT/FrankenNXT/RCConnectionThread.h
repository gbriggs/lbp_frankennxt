#pragma once

#include "TCPServerThread.h"
#include "RobotControlThread.h"


class Logging;
class NavigationEngine;

//  RCConnectionThread
//  Opens up TCPIP connection on port 48889 and listens for commands to control the robot
//
class RCConnectionThread : public TCPServerThread
{
public:

	RCConnectionThread(Logging& logging, NavigationEngine& navEngine, Motor& motor2, Motor& motor3);
	virtual ~RCConnectionThread();

	bool StartRCConnectionThread();
	
	virtual void RunFunction();

protected:

	RobotControlThread _RobotControlThread;
	
	Logging& _Logging;
	NavigationEngine& _NavEngine;
	
	
	

};

