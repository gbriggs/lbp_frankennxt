#ifndef _CMDIWLISTSCAN_H
#define _CMDIWLISTSCAN_H

#include <string>
#include <map>
#include <vector>

#include "CMD.h"



//  ConnectionInfo
//  container class for internet information
//
class WiFiAccessPoint
{
public:

	WiFiAccessPoint()
	{
		Address = "";
		Essid = "";
		Protocol = "";
		Frequency = "";
		Mode = "";
		Encrypted = false;
		BitRate = "";
		Quality = 0;
		SignalLevel = 0;
	}

	bool IsVisible() { return Quality > 10 && SignalLevel > 10; }

	std::string Address;
	std::string Essid;
	std::string Protocol;
	std::string Frequency;
	std::string Mode;
	bool   Encrypted;
	std::string BitRate;
	int Quality;
	int SignalLevel;
};



//  deletion helper function
//inline static bool deleteWiFiAccessPoint(WiFiAccessPoint* objectToDelete) { delete objectToDelete; return true; }


// CMDifconfig
// Command "ifconfig"
//
class CMDiwlistScan : public CMD
{
public:
	CMDiwlistScan();
	CMDiwlistScan(CMDiwlistScan& rhs);

	virtual ~CMDiwlistScan();

	CMDiwlistScan& operator=(CMDiwlistScan& rhs);

	//  override base class CMD::Parse so we can parse internet address out of system response
	virtual bool Parse();

	std::vector<std::string> AccessPoints();
	std::vector<std::string> VisibleAccessPoints();
	WiFiAccessPoint* AccessPoint(std::string name);

protected:
	std::map<std::string, WiFiAccessPoint*> _AccessPoints;
};


#endif