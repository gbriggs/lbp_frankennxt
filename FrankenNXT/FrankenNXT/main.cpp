#include <iostream>
#include <algorithm>
#include <WiringPiExtension.h>
#include "TheApp.h"




using namespace std;

//  Map of MCP chips, pin base to number of pins
map<int, int> McpMap;
// Map of Pca chips, pin base to file descriptor
map<int, int> PcaMap;

//  Logging object
Logging _Logging;

//  The main application object
TheApp theApp(_Logging);




int main(int argc, char *argv[])
{
	//  Setup WiringPi using GPIO pin numbers
	int retVal = SetupWiringPiExtensionGpio();

	//  Adafruit DC Motor Hat at default address - pin numbers 100 - 115
	int fd = Pca9685Setup(100, 0x60, 50.0);
	if (fd != 0) 
	{
		// Reset all output 
		Pca9685PWMReset(fd); 
		PcaMap[100] = fd;
	}
	
	//  MCP 23008 at 0x20 - pin numbers 200 - 207
	Mcp23008Setup(200, 0x20);
	McpMap[200] = 8;
	
	//  Initialize MCP as all output
	map<int, int>::iterator it;
	for (it = McpMap.begin(); it != McpMap.end(); it++)
	{
		int pinBase = it->first;
		int numPins = it->second;
		for (int i = 0; i < numPins; i++)
			PinMode(i + pinBase, PINMODE_OUTPUT);
	}
	

	//  startup the app
	if (!theApp.Start())
	{
		//  failure here is probably related to problem opening socket port in connection thread
		//  this is fatal error, bail out
		printf("! Failed to start application.");
		return 1;
	}
	

	while ( theApp.IsRunning())
	{
		string input;
		getline(cin, input);
		transform(input.begin(), input.end(), input.begin(), ::toupper); 
		
		if (input.compare("Q") == 0)
		{
			return 0;
		}
		else if (input.compare("W") == 0)
		{
			theApp.LogWiFiStatusToTerminal();
		}
 
	}
}