#include <sys/time.h>
#include <vector>

#include "WiFiControlThread.h"
#include "WiFiHandler.h"
#include "Parser.h"

using namespace std;





//  Constructor
//
WiFiControlThread::WiFiControlThread(WiFiHandler& wiFiHandler)
	: _WiFiHandler(wiFiHandler)
{
	Notified = false;
}


//  Destructor
//
WiFiControlThread::~WiFiControlThread()
{
	if ( ThreadRunning )
	{
		Cancel();
	}

	
}


//  signal the control thread to do something
//  will return false if it is busy doing a command already
bool WiFiControlThread::ControlCommand(string command)
{
	if (_IsBusy)
		return false;

	_IsBusy = true;

	Command = command;

	//  notify thread we have a command
	Notify();

	return true;
}




//  Cancel
//
void WiFiControlThread::Cancel()
{
	ThreadRunning = false;
	
	Notify();

	Thread::Cancel();
}


//  Notify
//  sets the notification flag and notifies the condition variable
//
void WiFiControlThread::Notify()
{
	Notified = true;
	NotifyMessagesCondition.notify_one();
}


//  RunFunction
//  the thread run function, will wait on condition
//  and wake up when there is a command to process
//
void WiFiControlThread::RunFunction()
{
	//  wait for notification of command to process
	std::unique_lock<std::mutex> lockNotify(NotifyMutex);

	//  avoid spurious wakeups
	while (!Notified)
		NotifyMessagesCondition.wait(lockNotify);

	//  check to see if we were woken up because of shutdown
	if (!ThreadRunning)
		return;
	
	//  do something with this command
	Parser commandParser(Command, ",");
	string command = commandParser.GetNextString();

	//  Look for recognized commands
	//
	if (command.compare("$LBP_SET_AP") == 0)
	{
		string response = "";

		//  get the arguments, access point and password
		Parser argParser(commandParser.GetRemainingBuffer(), ",");
		
		string accessPoint = argParser.GetNextString();
		string password = argParser.GetNextString();

		_WiFiHandler.SetAccessPoint(accessPoint, password);
	}
	else if (command.compare("$LBP_SET_MODE") == 0)
	{
		//  get the arguments, access point and password
		Parser argParser(commandParser.GetRemainingBuffer(), ",");

		string modeString = argParser.GetNextString();

		iwcMode mode = iwcUnknown;

		//  validate input
		if (modeString.compare("Master") == 0)
			mode = iwcMaster;
		else if (modeString.compare("Managed") == 0)
			mode = iwcManaged;

		_WiFiHandler.SetMode(mode);
	}
	else
	{
		//  bad dog !

	}
	
	//  we are done, wait a bit before allowing another command
	Sleep(5000);

	_IsBusy = false;
	Notified = false;

	return;
}