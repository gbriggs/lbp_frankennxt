#pragma once

#include "Thread.h"

class Logging;
class NavigationEngine;

//  IMU Thread
//  Initializes RTIMU library, and gets latest IMU readings in a thread

class ImuThread : public Thread
{
public:

	ImuThread(Logging& logging, NavigationEngine& navEngine);
	
	virtual ~ImuThread();

		
	//  override of Thread::Cancel so we can wake thread up before stopping run function
	virtual void Cancel();

	//  RunFunction
	virtual void RunFunction();

protected:

	Logging& _Logging;
	NavigationEngine& _NavigationEngine;

	
};



