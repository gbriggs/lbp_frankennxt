//  GenericClass.cpp

#include "Vehicle.h"
#include <WiringPiExtension.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
using namespace std;

#define MAX_SPEED_TO_SPIN 95
#define SPEED_FOR_SPIN 50
#define DIRECTION_FOR_SPIN 30


//  Constructor
//
Motor::Motor()
{
	Pwm = 0;
	Direction1 = 0;
	Direction2 = 0;
}

//  Destructor
//
Motor::~Motor()
{
}


//  Set motor pins, and setup pin modes
//
void Motor::SetupMotor(int pwm, int dir1, int dir2)
{
	Pwm = pwm;
	Direction1 = dir1;
	Direction2 = dir2;
	
	PinMode(Pwm, PINMODE_PWM_OUTPUT);
	PinMode(Direction1, PINMODE_PWM_OUTPUT);
	PinMode(Direction2, PINMODE_PWM_OUTPUT);
}


//  Turn motor on
//
void Motor::On(int speed)
{
	if (speed > 0)
	{
		DigitalWrite(Direction1, 1);
		DigitalWrite(Direction2, 0);
	}
	else
	{
		DigitalWrite(Direction1, 0);
		DigitalWrite(Direction2, 1);
	}
	
	int value = (double)speed / 100 * 4096;
	PwmWrite(Pwm, abs(value));
	
	Speed = speed;
	
}



/////////////////////////////////////////
//  Vehicle Class
//
Vehicle::Vehicle(Motor& leftMotor, Motor& rightMotor) :
	LeftMotor(leftMotor), RightMotor(rightMotor)
{
	
}


//  Destructor
//
Vehicle::~Vehicle()
{
}


//  Driving command
//
void Vehicle::Drive(int speed, int direction)
{
	bool spinNeeded = false;
	if (abs(direction) >= MAX_SPEED_TO_SPIN)
		spinNeeded = true;
	
	if (0 == speed)
	{
		LeftMotor.On(0);
		RightMotor.On(0);	
	}	
	else if (direction == 0)
	{
		LeftMotor.On(speed);
		RightMotor.On(speed);
	}
	else if (abs(direction) >= MAX_SPEED_TO_SPIN)
	{
		if (direction < 0) // spin to left
		{
			LeftMotor.On(-speed);
			RightMotor.On(speed);
		}
		else
		{
			LeftMotor.On(speed);
			RightMotor.On(-speed);
		}
	}
	else if (direction<0)
	{
		LeftMotor.On( (int) (speed * ((double)abs(direction)/100.0) ));
		RightMotor.On(speed);	
	}
	else
	{
		LeftMotor.On(speed);	
		RightMotor.On((int)(speed * ((double)abs(direction) / 100.0)) ) ;
	}
	
	return;
}


//  Left Track On
//
void Vehicle::LeftTrackOn(int speed)
{
	LeftMotor.On(speed);
}


//  Right Track On
//
void Vehicle::RightTrackOn(int speed)
{
	RightMotor.On(speed);
}

