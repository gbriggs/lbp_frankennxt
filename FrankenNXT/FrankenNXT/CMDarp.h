#ifndef _CMD_ARP
#define _CMD_ARP

#include <string>
#include <vector>
#include <map>

#include "CMD.h"



#define ROUTERMODE_BASEADDRESS "192.168.150."


 
class ArpDevice
{
public:

	ArpDevice()
	{
		IpAddress = "";
		HwAddress = "";
	}

	std::string IpAddress;
	std::string HwAddress;
};


/////////////////////////////////////////////////////////////////////////////
//  CMDarpWiFiDevices
//  Command "arp"
//  parses out devices connected on wlan0, creates map of hardware address to device object
//
class CMDarpWiFiDevices : public CMD
{
public:
	CMDarpWiFiDevices();
	CMDarpWiFiDevices(CMDarpWiFiDevices& rhs);

	virtual ~CMDarpWiFiDevices();

	CMDarpWiFiDevices& operator=(CMDarpWiFiDevices& rhs);

	//  override base class CMD::Parse so we can parse internet address out of system response
	virtual bool Parse();

	std::vector<std::string> ConnectedDevices();
	ArpDevice* ConnectedDevice(std::string name);
	
protected:

	std::map<std::string, ArpDevice*>  _ConnectedDevices;
};




//  CMDipNeighShow
//  Command "ip neigh show"
//  parses out devices connected on wlan0, creates map of hardware addresses to device object
//
class CMDipNeighShow : public CMD
{
public:
	CMDipNeighShow();
	CMDipNeighShow(CMDipNeighShow& rhs);

	virtual ~CMDipNeighShow();

	CMDipNeighShow& operator=(CMDipNeighShow& rhs);

	virtual bool Parse();

	std::vector<std::string> ConnectedDevices();
	ArpDevice* ConnectedDevice(std::string name);

protected:
	std::map<std::string, ArpDevice*>  _ConnectedDevices;
};

#endif