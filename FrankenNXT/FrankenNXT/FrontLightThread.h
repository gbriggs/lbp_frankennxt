#pragma once

#include "Thread.h"


class NavigationEngine;

//  Front Light Thread
//  controls the front light flashing state based on distance to obstacles in front of the robot
//
class FrontLightThread : public Thread
{
public:

	FrontLightThread(NavigationEngine& navEngine);
	
	virtual ~FrontLightThread();

	virtual void Cancel();

	virtual void RunFunction();

protected:

	NavigationEngine& _NavigationEngine;

	int BlueLightStartPin;
	int BlueLightMidPin;
	int BlueLightEndPin;
	
	//  lights control
	void AllOff();
	void CylonLights(int value, bool right);
	bool GoingRight;
	
};



