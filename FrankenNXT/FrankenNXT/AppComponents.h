#pragma once

class Logging;
class WiFiHandler;
class NavigationEngine;
class Vehicle;
class Motor;

//  Container Class to pass around app components
class AppComponents
{
public:
	AppComponents( WiFiHandler& wiFiHandler, NavigationEngine& navEngine, Vehicle& vehicle, Motor& motor2, Motor& motor3);
	
	WiFiHandler& _WiFiHandler;
	NavigationEngine& _NavigationEngine;
	Vehicle& _Vehicle;
	Motor& _Motor2;
	Motor& _Motor3;
};