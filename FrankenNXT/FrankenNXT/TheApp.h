#pragma once

#include <string>
#include <list>
#include <mutex>
#include <map>

#include "Logging.h"
#include "PulseThread.h"
#include "WiFiHandler.h"
#include "RobotControlThread.h"
#include "RCConnectionThread.h"
#include "Vehicle.h"
#include "ClientConnectionThread.h"
#include "ClientControlThread.h"
#include "DistanceSensorThread.h"
#include "ImuThread.h"
#include "WheelEncoderThread.h"
#include "NavigationEngine.h"
#include "AppComponents.h"


// TheApp
// The main application class

class TheApp
{
public:

	TheApp(Logging& logging);
	virtual ~TheApp();

	//  initialize app components for startup
	bool Start();

	//  shut down application components for exit
	void ShutDown();
	
	//  Public API
	//
	bool IsRunning();
	
	void PauseWriteLogsToTerminal();
	
	void ResumeWriteLogsToTerminal();
	
	void LogWiFiStatusToTerminal();
	
	
protected:
	
	//  Wi-Fi handler includes query / post on port 48888
	WiFiHandler _WiFiHandler;
	timeval TimeOfLastConnection;
	
	//  Remote driving commands are posted to port 48889
	RCConnectionThread _RCConnectionThread;
	
	//  Connected clients can query / post on port 48890
	ClientConnectionThread _ClientConnectionThread;
	
	
	//  Sensors
	DistanceSensorThread _DistanceSensorThread;
	ImuThread _ImuThread;
	WheelEncoderThread _WheelEncoderThread;
	
	//  Motors
	//  motor 2 and 3 are independent connections
	Motor _Motor2;
	Motor _Motor3;
	//  motor 1 and 4 are defined in the Vehicle class
	Motor _Motor1;
	Motor _Motor4;
	Vehicle _Vehicle;
	
	//  Navigation engine processes
	NavigationEngine _NavigationEngine;

	//  container for app components
	AppComponents _AppComponents;
	
	//  Utility
	PulseThread _PulseThread;
	Logging& _Logging;
	
	std::string VersionString;
};


#define SYSEVENT "systemEvent"

