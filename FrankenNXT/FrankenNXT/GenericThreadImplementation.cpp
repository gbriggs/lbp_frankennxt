#include <sys/time.h>


#include "GenericThreadImplementation.h"



using namespace std;


//  Constructor
//
GenericThreadImplementation::GenericThreadImplementation()
	
{
	
}


//  Destructor
//
GenericThreadImplementation::~GenericThreadImplementation()
{
	if ( ThreadRunning )
	{
		Cancel();
	}

	
}




//  Cancel
//
void GenericThreadImplementation::Cancel()
{
	ThreadRunning = false;
	

	Thread::Cancel();
}



//  RunFunction
//  the thread run function, will wait on condition
//  and wake up when there is a command to process
//
void GenericThreadImplementation::RunFunction()
{
	while (ThreadRunning)
	{
		//  we are done, wait a bit before allowing another command
		Sleep(5000);
	}
	

	return;
}