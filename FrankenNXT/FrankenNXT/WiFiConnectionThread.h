#pragma once

#include "TCPServerThread.h"


class WiFiHandler;




//  WiFiConnectionThread
//  Opens up TCPIP connection on port 48888 and listens for query / command of Wi-Fi state
//  this is the inter process comms channel for website pages to get to wifi state info and issue wifi state change commands
//
class WiFiConnectionThread : public TCPServerThread
{
public:

	WiFiConnectionThread(WiFiHandler& wiFiHandler);
	virtual ~WiFiConnectionThread();

	//  TCP server is running in the RunFunction
	virtual void RunFunction();

protected:

	WiFiHandler& _WiFiHandler;
};


