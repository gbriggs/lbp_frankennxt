#include <sys/time.h>
#include <vector>
#include <algorithm>

#include "ClientControlThread.h"
#include "Logging.h"
#include "Parser.h"

using namespace std;




//  Constructor
//
ClientControlThread::ClientControlThread(Logging& logging)
	: _Logging(logging)
{
	Notified = false;
}


//  Destructor
//
ClientControlThread::~ClientControlThread()
{
	if ( ThreadRunning )
	{
		Cancel();
	}

	
}






//  Cancel
//
void ClientControlThread::Cancel()
{
	ThreadRunning = false;
	
	Notify();

	Thread::Cancel();
}


//  Notify
//  sets the notification flag and notifies the condition variable
//
void ClientControlThread::Notify()
{
	Notified = true;
	NotifyQueueCondition.notify_one();
}



void ClientControlThread::AddCommandToQueue(string command)
{
	//  add string to a list
	{
		LockMutex lockQueue(QueueMutex);
		CommandQueue.push(command);
	}


	//  notify thread we have a command
	Notify();

	
}


//  RunFunction
//  
void ClientControlThread::RunFunction()
{
	if (CommandQueue.size() == 0 && ThreadRunning)
	{
		//  wait for messages
		std::unique_lock<std::mutex> lockNotify(NotifyMutex);

		//  avoid spurious wakeups
		while(!Notified)
			NotifyQueueCondition.wait(lockNotify);

		//  check to see if we were woken up because of shutdown
		if(!ThreadRunning)
			return;
	}

	list<string> commands;
	//  empty the queue and put the events to send into a list
	{
		LockMutex lockQueue(QueueMutex);
		
		while (CommandQueue.size() > 0)
		{
			string nextCommand = CommandQueue.front();
			commands.push_back(nextCommand);
			CommandQueue.pop();
		}
	}
	
	//  process everything in the queue
	list<string>::iterator nextItem;
	for (nextItem = commands.begin(); nextItem != commands.end(); ++nextItem)
	{
		//  do something
	}
		
	

	Notified = false;
}