#pragma once

#include "Thread.h"

class WiFiHandler;

//  NetworkMonitorThread
//  a thread running to monitor the state of the network connection
//
class NetworkMonitorThread : public Thread
{
public:
	
	NetworkMonitorThread(WiFiHandler& wiFiHandler);

	virtual void RunFunction();
	
	void LogWiFiStatusToTerminal();

	timeval TimeOfLastConnection;
	timeval TimeOfLastSwitchToRouterMode;

protected:
	
	WiFiHandler& _WiFiHandler;
}
;

