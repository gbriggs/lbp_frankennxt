#pragma once

#include "Thread.h"

class Logging;
class NavigationEngine;

//  Distance Sensor Class
//  Initializes VL53L0x module and queries measurements in a therad
class DistanceSensorThread : public Thread
{
public:

	DistanceSensorThread(Logging& logging, NavigationEngine& navEngine);
	
	virtual ~DistanceSensorThread();

	virtual void Start();
	
	//  override of Thread::Cancel so we can wake thread up before stopping run function
	virtual void Cancel();

	//  RunFunction
	virtual void RunFunction();

protected:

	Logging& _Logging;
	NavigationEngine& _NavigationEngine;
	int SensorIndex;


	
};



