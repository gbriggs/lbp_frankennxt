#pragma once

#include <condition_variable>
#include <queue>
#include <list>

#include "Thread.h"


class Logging : public Thread
{
public:

	Logging();
	
	virtual ~Logging();

	//  override of Thread::Cancel so we can wake thread up before stopping run function
	virtual void Cancel();

	//  RunFunction
	virtual void RunFunction();
	
	void Log(std::string command);
	
	void PauseWriteToTerminal();
	void ResumeWriteToTerminal();
	

protected:
	
	std::mutex QueueMutex;
	std::queue<std::string> CommandQueue;

	
	
	//  condition variable is used for notification of thread to wake up
	bool Notified;
	std::condition_variable NotifyQueueCondition;
	std::mutex NotifyMutex;

	void Notify();
	
	bool WriteToTerminal;
};


