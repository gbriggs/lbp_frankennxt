#pragma once

#ifdef __cplusplus
extern "C" {
#endif

	//  initialize device
	int vl53l0xStartDevice();

	//  get device reading in mm
	int vl53l0xDeviceReading(int device);

#ifdef __cplusplus
}
#endif
