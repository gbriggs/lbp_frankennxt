#pragma once

#include "Thread.h"
#include "vl53l0x_api.h"
#include "vl53l0x_platform.h"



//
//  ControlThread
//  a thread that waits for messages to be added to the queue,
//  then broadcasts messages to all clients
//

class VL53L0XThread : public Thread
{
public:

	VL53L0XThread();
	
	virtual ~VL53L0XThread();

	int Initialize(int address);
		
	//  override of Thread::Cancel so we can wake thread up before stopping run function
	virtual void Cancel();

	//  RunFunction
	virtual void RunFunction();

	volatile int Measurement;

protected:

	VL53L0X_Dev_t VL53Device;
	VL53L0X_Version_t Version;
	VL53L0X_DeviceInfo_t   DeviceInfo;

	

	
};



