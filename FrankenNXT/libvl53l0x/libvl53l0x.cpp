#include <map>
#include <iostream>
#include "libvl53l0x.h"
#include "vl53l0x_api.h"
#include "vl53l0x_platform.h"
#include "vl53l0xThread.h"




using namespace std;

static map < int, VL53L0XThread*> VL53L0XMap;




//  initialize device
//  initialize device
int vl53l0xStartDevice()
{
	VL53L0XThread *newSensor = new VL53L0XThread();

	int sensorFd = newSensor->Initialize(0x29);
	if (sensorFd > 0)
	{
		VL53L0XMap[sensorFd] =  newSensor;
		newSensor->Start();
		return sensorFd;
	}
	else
	{
		delete newSensor;
		return -1;
	}
}

//  get device reading in mm
int vl53l0xDeviceReading(int device)
{
	//  Find the step driver
	map<int, VL53L0XThread*>::iterator it = VL53L0XMap.find(device);
	if (it != VL53L0XMap.end())
	{
		return it->second->Measurement;
	}
}