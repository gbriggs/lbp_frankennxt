#include <sys/time.h>
#include <unistd.h>

#include "vl53l0xThread.h"

#include <cstdio>
#include <iostream>
#include "libvl53l0x.h"
#include "vl53l0x_api.h"
#include "vl53l0x_platform.h"


using namespace std;


#define VERSION_REQUIRED_MAJOR 1
#define VERSION_REQUIRED_MINOR 0
#define VERSION_REQUIRED_BUILD 1



void print_pal_error(VL53L0X_Error Status) {
	char buf[VL53L0X_MAX_STRING_LENGTH];
	VL53L0X_GetPalErrorString(Status, buf);
	printf("API Status: %i : %s\n", Status, buf);
}


//  Constructor
//
VL53L0XThread::VL53L0XThread()
	
{
	
}


//  Destructor
//
VL53L0XThread::~VL53L0XThread()
{
	if (ThreadRunning)
	{
		Cancel();
	}

	
}




//  Cancel
//
void VL53L0XThread::Cancel()
{
	ThreadRunning = false;
	

	Thread::Cancel();
}



int VL53L0XThread::Initialize(int address)
{
	VL53L0X_Error Status = VL53L0X_ERROR_NONE;
	
	uint8_t VhvSettings;
	uint8_t PhaseCal;
	uint32_t refSpadCount;
	uint8_t isApertureSpads;
	int32_t status_int;

	printf("VL53L0X PAL Continuous Ranging example\n\n");

	    // Initialize Comms
	VL53Device.I2cDevAddr      = address;

	VL53Device.fd = VL53L0X_i2c_init("/dev/i2c-1", VL53Device.I2cDevAddr); //choose between i2c-0 and i2c-1; On the raspberry pi zero, i2c-1 are pins 2 and 3
	if (VL53Device.fd < 0) 
	{
		Status = VL53L0X_ERROR_CONTROL_INTERFACE;
		cout << "Failed to init VL53L0X" << endl;
		return -1;
	}

	    /*
	     *  Get the version of the VL53L0X API running in the firmware
	     */

	if (Status == VL53L0X_ERROR_NONE)
	{
		status_int = VL53L0X_GetVersion(&Version);
		if (status_int != 0)
		{
			Status = VL53L0X_ERROR_CONTROL_INTERFACE;
			
			return -1;
		}
	}

			    /*
			     *  Verify the version of the VL53L0X API running in the firmrware
			     */

//	if (Status == VL53L0X_ERROR_NONE)
//	{
//		if (Version.major != VERSION_REQUIRED_MAJOR ||
//		    Version.minor != VERSION_REQUIRED_MINOR ||
//		    Version.build != VERSION_REQUIRED_BUILD)
//		{
//			printf("VL53L0X API Version Error: Your firmware has %d.%d.%d (revision %d). This example requires %d.%d.%d.\n",
//				Version.major,
//				Version.minor,
//				Version.build,
//				Version.revision,
//				VERSION_REQUIRED_MAJOR,
//				VERSION_REQUIRED_MINOR,
//				VERSION_REQUIRED_BUILD);
//			return -1;
//		}
//	}

				    // End of implementation specific
	if (Status == VL53L0X_ERROR_NONE)
	{
		printf("Call of VL53L0X_DataInit\n");
		Status = VL53L0X_DataInit(&VL53Device); // Data initialization
		print_pal_error(Status);
		
	}
    
	if (Status == VL53L0X_ERROR_NONE)
	{
		Status = VL53L0X_GetDeviceInfo(&VL53Device, &DeviceInfo);
	}
	if (Status == VL53L0X_ERROR_NONE)
	{
		printf("VL53L0X_GetDeviceInfo:\n");
		printf("Device Name : %s\n", DeviceInfo.Name);
		printf("Device Type : %s\n", DeviceInfo.Type);
		printf("Device ID : %s\n", DeviceInfo.ProductId);
		printf("ProductRevisionMajor : %d\n", DeviceInfo.ProductRevisionMajor);
		printf("ProductRevisionMinor : %d\n", DeviceInfo.ProductRevisionMinor);

		if ((DeviceInfo.ProductRevisionMinor != 1) && (DeviceInfo.ProductRevisionMinor != 1)) 
		{
			printf("Error expected cut 1.1 but found cut %d.%d\n",
				DeviceInfo.ProductRevisionMajor,
				DeviceInfo.ProductRevisionMinor);
			Status = VL53L0X_ERROR_NOT_SUPPORTED;
			return -1;
		}
	}

	if (Status == VL53L0X_ERROR_NONE)
	{
		printf("Call of VL53L0X_StaticInit\n");
		Status = VL53L0X_StaticInit(&VL53Device); // Device Initialization
		// StaticInit will set interrupt by default
		print_pal_error(Status);
	}
	if (Status == VL53L0X_ERROR_NONE)
	{
		printf("Call of VL53L0X_PerformRefCalibration\n");
		Status = VL53L0X_PerformRefCalibration(&VL53Device,
			&VhvSettings,
			&PhaseCal); // Device Initialization
		print_pal_error(Status);
	}

	if (Status == VL53L0X_ERROR_NONE)
	{
		printf("Call of VL53L0X_PerformRefSpadManagement\n");
		Status = VL53L0X_PerformRefSpadManagement(&VL53Device,
			&refSpadCount,
			&isApertureSpads); // Device Initialization
		print_pal_error(Status);
	}

	if (Status == VL53L0X_ERROR_NONE)
	{

		printf("Call of VL53L0X_SetDeviceMode\n");
		Status = VL53L0X_SetDeviceMode(&VL53Device, VL53L0X_DEVICEMODE_CONTINUOUS_RANGING); // Setup in single ranging mode
		print_pal_error(Status);
	}
    
	if (Status == VL53L0X_ERROR_NONE)
	{
		printf("Call of VL53L0X_StartMeasurement\n");
		Status = VL53L0X_StartMeasurement(&VL53Device);
		print_pal_error(Status);
	}

	if (Status == VL53L0X_ERROR_NONE)
	{
		return VL53Device.fd;
	}
	else
	{
		return -1;
	}
}

VL53L0X_Error WaitMeasurementDataReady(VL53L0X_DEV Dev) {
	VL53L0X_Error Status = VL53L0X_ERROR_NONE;
	uint8_t NewDatReady = 0;
	uint32_t LoopNb;

	    // Wait until it finished
	    // use timeout to avoid deadlock
	if (Status == VL53L0X_ERROR_NONE) {
		LoopNb = 0;
		do {
			Status = VL53L0X_GetMeasurementDataReady(Dev, &NewDatReady);
			if ((NewDatReady == 0x01) || Status != VL53L0X_ERROR_NONE) {
				break;
			}
			LoopNb = LoopNb + 1;
			VL53L0X_PollingDelay(Dev);
		} while (LoopNb < VL53L0X_DEFAULT_MAX_LOOP);

		if (LoopNb >= VL53L0X_DEFAULT_MAX_LOOP) {
			Status = VL53L0X_ERROR_TIME_OUT;
		}
	}

	return Status;
}

//  RunFunction
//  the thread run function, will wait on condition
//  and wake up when there is a command to process
//
void VL53L0XThread::RunFunction()
{
	VL53L0X_RangingMeasurementData_t    RangingMeasurementData;
	VL53L0X_RangingMeasurementData_t   *pRangingMeasurementData    = &RangingMeasurementData;
	VL53L0X_Error Status = VL53L0X_ERROR_NONE;
	

	Status = WaitMeasurementDataReady(&VL53Device);

	if (Status == VL53L0X_ERROR_NONE)
	{
		Status = VL53L0X_GetRangingMeasurementData(&VL53Device, pRangingMeasurementData);

		Measurement = pRangingMeasurementData->RangeMilliMeter;
		//printf("In loop measurement %d: %d\n", measurement, pRangingMeasurementData->RangeMilliMeter);
			                
		// Clear the interrupt
		VL53L0X_ClearInterruptMask(&VL53Device, VL53L0X_REG_SYSTEM_INTERRUPT_GPIO_NEW_SAMPLE_READY);
		usleep(10000);
	}
			
		

}