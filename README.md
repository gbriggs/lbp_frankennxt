# README #

frankenNXT is a Raspberry Pi robot control system. 

### System Components ###

* A C++ program that runs on a Raspberry Pi with an Adafruit motor hat that can receive commands over TCPIP to control the robot.
* Scripts and config files to enable the Pi to be either a Wi-Fi hotspot or to join a local Wi-Fi network so you can always connect to the Raspberry Pi without a keyboard or monitor.
* A web user interface for the Pi to access Wi-Fi settings, system status, and pages to view and control the robot's webcam.
* A controller program that runs on another Linux machine where you can plug in the XBox remote and drive the robot.


* [More details on our website at littlebytesofpi.com/frankennxt](http://littlebytesofpi.com/frankennxt)
