﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Timers;
using SimpleJoy;
using System.Xml;

namespace MonoRobot
{
    // MonoRobot
    


    /// <summary>
    /// Robot class
    /// interface point between client application and the remote robot
    /// </summary>
    public partial class Robot
    {
        //
        // Public Interfaces
        //
        #region PublicInterfaces
        
        //  Robot Events
        //
        public event ConnectionEventHandler ConnectionStateChanged;
        public event MotorEventHandler MotorStateChanged;
        public event SensorEventHandler SensorStateChanged;

        //  Monitor
        public bool MonitorMotorSpeed { get; set; }
        public bool MonitorMotorTach { get; set; }
        public bool MonitoringMotors { get { return MonitorMotorSpeed || MonitorMotorTach; } }
        
        //  Sensors
        public bool MonitorSensors { get { return MonitorColorSensor || MonitorIrSensor || MonitorTouchSensor; } }
        public bool MonitorColorSensor { get; set; }
        public bool MonitorIrSensor { get; set; }
        public bool MonitorTouchSensor { get; set; }


        //  Motors
        //  Access brick motors by index
        //  index 0 - 3 for first brick in chain, 4-7 for second ...
        //
        public int MotorCount
        {
            get
            {
                return Motors.Count;
            }
        }


        /// <summary>
        /// Get the speed of the motor
        /// </summary>
        /// <param name="index">index of the motor</param>
        public int GetMotorSpeed(int index)
        {
            if (Motors.Count > 0 && Motors.Count > index)
            {
                return Motors[index].Speed;
            }
            return 0;
        }


        /// <summary>
        /// Get the brick port the motor is connected to
        /// will be port A - D
        /// </summary>
        /// /// <param name="index">index of the motor</param>
        public string GetMotorPort(int index)
        {
            if (Motors.Count > 0 && Motors.Count > index)
            {
                return Motors[index].ConnectedPort;
            }
            return "not connected";
        }


        /// <summary>
        /// Get the tacho count for the motor 
        /// </summary>
        /// <param name="index">index of the motor</param>
        public int GetMotorTachoCount(int index)
        {
            if (Motors.Count > 0 && Motors.Count > index)
            {
                return Motors[index].TachoCount;
            }
            return 0;
        }


        /// <summary>
        /// Set Motor
        /// sets the motor to the speed speed
        /// </summary>
        /// <param name="index">index of the motor</param>
        /// <param name="speed">motor speed from -100 to 100</param>
        public void SetMotor(int index, int speed)
        {
            if (FrankenNXT == null || !IsConnected)
                return;

            ProcessSetMotor(index, speed);
        }


        /// <summary>
        /// Turn Motor
        /// turns the motor the specified number of degrees
        /// </summary>
        /// <param name="index">index to the motor</param>
        /// <param name="degrees"> +/-ve degrees to turn</param>
        public void TurnMotor(int index, int degrees, int speed)
        {
            if (FrankenNXT == null || !IsConnected)
                return;

            ProcessTurnMotor(index, degrees, speed);
        }


        /// <summary>
        /// Turn off the motor
        /// </summary>
        /// <param name="index">index of the motor</param>
        public void MotorOff(int index, bool resetTach = false)
        {
            if (FrankenNXT == null || !IsConnected)
                return;

            ProcessMotorOff(index, resetTach);
        }


        //  Sensors
        //  Access sensors by index
        //  index 0-3 for first brick in chain, 4-7 for second ...
        //
        public int SensorCount
        {
            get
            {
                return Sensors == null ? 0 : Sensors.Count;
            }
        }

        /// <summary>
        /// Get the name of the sensor
        /// </summary>
        /// <param name="index">index of the sensor</param>
        public string GetSensorName(int index)
        {
            if (Sensors.Count > 0 && Sensors.Count > index)
            {
                return Sensors[index].Name;
            }
            return "---";
        }


        

 
        /// <summary>
        /// Get the sensor type
        /// </summary>
        /// <param name="index">index of the sensor</param>
        public SensorMode GetSensorType(int index)
        {
            return SensorMode.Unknown;
        }


        /// <summary>
        /// Get the latest reading from the sensor
        /// </summary>
        /// <param name="index">index of the sensor</param>
        public string GetSensorLatestReading(int index)
        {
            if (Sensors.Count > 0 && Sensors.Count > index)
            {
                return Sensors[index].LatestReading;
            }
            return "---";
        }


     



        //  Joystick Controller Input
        //

        public enum XboxJoystickMode
        {
            TrackSteer,
            RightStick,
            outOfRange
        };

        public XboxJoystickMode XBoxJoystickUsageMode { get; set; }

      

        /// <summary>
        /// XBox Controller Input
        /// </summary>
        public void XboxDrigingInput(XBoxJoystickEventArgs data)
        {
            switch ( XBoxJoystickUsageMode )
            {
                case XboxJoystickMode.TrackSteer:
                    ProcessXboxControllerInputTrackSteering(data);
                    break;

                case XboxJoystickMode.RightStick:
                    ProcessXboxControllerInputRightStick(data);
                    break;
            }
            
        }

        /// <summary>
        /// Keyboard Input
        /// </summary>
        public void KeyboardDrivingInput(int key)
        {
            if (FrankenNXT == null || !IsConnected)
                return;

            ProcessKeyboardDrivingInput(key);
        }


       

      
        #endregion //  Public Interfaces


        //
        //  Construction and Initialization
        //
        #region Construction and Initialization

        /// <summary>
        /// Constructor
        /// </summary>
        public Robot()
        {
            InitializeRobot();

            //  default start monitor everything
            MonitorMotorSpeed = true;
            MonitorMotorTach = true;
            MonitorColorSensor = true;
            MonitorTouchSensor = true;
            MonitorIrSensor = true;

            //  Status Timer to request status updates
            StatusTimer = new System.Timers.Timer();
            StatusTimer.Elapsed += new ElapsedEventHandler(OnStatusTimer);
            StatusTimer.Interval = 50;
            StatusTimer.Enabled = true;

            //  Communication requests go through a queue serviced by this thread
            RunCommandQueueThread = true;
            CommandQueueThread = new Thread(new ThreadStart(CommandQueueThreadRun));
            CommandQueueThread.Start();

            //  Connection Status
            _ConnectionStatus = ConnectionState.Disconnected;

            
            XBoxJoystickUsageMode = XboxJoystickMode.TrackSteer;

           
        }

      


        //  Status Timer
        //
        System.Timers.Timer StatusTimer;
        
        /// <summary>
        /// Status Timer tick
        /// </summary>
        private void OnStatusTimer(object source, ElapsedEventArgs e)
        {
            //  check to see if we are connected
            if (!IsConnected)
                return;

            //  check to see if we are monitoring anything
            if (!MonitoringMotors && !MonitorSensors)
                return;
            
            //  fire off a status event
            RobotCommand cmd = new RobotCommand(FrankenNXT, () =>
            {
                //  TODO something here
            });
            QueueCommand(cmd);
        }


         //
        //  Access to sensors
        private List<Sensor> Sensors { get; set; }
        private List<Motor> Motors { get; set; }

        //  Communication Lock
        private static Object LockCommandQueue = new Object();
        //
        //  Command Queue
        //
        //  use a AutoResetEvent to signal thread to process commands
        private AutoResetEvent NotifyData = new AutoResetEvent(false);
        //  commands go in this queue
        List<RobotCommand> CommandQueue = new List<RobotCommand>();

        /// <summary>
        /// Queue up a command to send to the robot
        /// </summary>
        void QueueCommand(RobotCommand command)
        {
            lock (LockCommandQueue)
            {
                CommandQueue.Insert(0, command);

                NotifyData.Set();
            }
        }

        /// <summary>
        /// Clear the command queue
        /// </summary>
        void ClearQueue()
        {
            lock (LockCommandQueue)
            {
                CommandQueue.Clear();
            }
        }


        /// <summary>
        /// Initialize Function
        /// </summary>
        protected void InitializeRobot()
        {
            Motors = new List<Motor>();
            Sensors = new List<Sensor>();

            FrankenNXT = new FrankenNXTWrapper();
            RobotLeftDriveMotor = new Motor("LeftTrack");
            RobotRightDriveMotor = new Motor("RightTrack");
            RobotMotor2 = new Motor("2");
            RobotMotor3 = new Motor("3");

            Motors.Add(RobotLeftDriveMotor);
            Motors.Add(RobotMotor2);
            Motors.Add(RobotMotor3);
            Motors.Add(RobotRightDriveMotor);

            //  Joystick handling
            LastJoystickDirection = JoystickDirection.Forward;
            LastJoystickSpeed = 0;
        }

        FrankenNXTWrapper FrankenNXT;

        //  Shut down
        //
        public void ShutDown()
        {
            DisconnectFromRobot();

            RunCommandQueueThread = false;
            if (CommandQueueThread.IsAlive)
            {
                //  send an interrupt
                CommandQueueThread.Interrupt();
                // wait for the thread run function to exit
                CommandQueueThread.Join();
            }
        }


       

        #endregion


        //
        //  Connection and Disconnection
        //
        #region Connection

        //  Connection State
        ConnectionState _ConnectionStatus;
        public ConnectionState ConnectionStatus
        {
            get
            {
                switch (_ConnectionStatus)
                {
                    case ConnectionState.Connected:
                        if (FrankenNXT == null || !FrankenNXT.IsConnected)
                            _ConnectionStatus = ConnectionState.Disconnected;
                        break;
                }
                return _ConnectionStatus;
            }
            protected set
            {
                _ConnectionStatus = value;
            }
        }

        public bool IsConnected { get { return ConnectionStatus == ConnectionState.Connected; } }

    
        /// <summary>
        /// Connection Helper Functions
        /// </summary>
        void StatusDisconnected()
        {
            _ConnectionStatus = ConnectionState.Disconnected;
            if (ConnectionStateChanged != null)
                ConnectionStateChanged(this, new ConnectionEventArgs(ConnectionState.Disconnected));
        }
        void StatusConnecting(string message)
        {
            _ConnectionStatus = ConnectionState.Connecting;
            if (ConnectionStateChanged != null)
                ConnectionStateChanged(this, new ConnectionEventArgs(ConnectionState.Connecting, message));
        }
        void StatusConnected()
        {
            _ConnectionStatus = ConnectionState.Connected;
            if (ConnectionStateChanged != null)
                ConnectionStateChanged(this, new ConnectionEventArgs(ConnectionState.Connected));
        }

        string ConnectionMethodString(string address)
        {
           
                    return "Wi-Fi";
             
            
        }

        /// <summary>
        /// Disconnect from the robot
        /// </summary>
        public bool DisconnectFromRobot()
        {
            if (FrankenNXT != null && FrankenNXT.IsConnected)
            {
                _ConnectionStatus = ConnectionState.Connecting;
                ClearQueue();
                try
                {
                    StatusDisconnected();
                    FrankenNXT.CloseConnection();
                    return true;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Set Motor caught exception: " + e.ToString());
                    return false;
                }
                finally
                {
                    //  TODO - shut down robot here ShutDownRobot();

                }
            }
            return false;
        }

        /// <summary>
        /// Connect to robot
        /// </summary>
        /// <param name="method">usb / bluetooth / wifi</param>
        /// <param name="address">com port for bt</param>
        public bool ConnectToRobot(string address)
        {
            if (FrankenNXT != null && FrankenNXT.IsConnected)
            {
                return false; //  bad state to call this - todo error handling
            }

            //  Init the robot state
            ClearQueue();

            //  Start connection
            _ConnectionStatus = ConnectionState.Connecting;

            //  update the state event
            StatusConnecting(string.Format("Connecting to {0}", ConnectionMethodString(address)));
            try
            {
                int maxRetries = 2;
                int connectionRetries = 0;
                while (!FrankenNXT.IsConnected)
                {
                    try
                    {
                        FrankenNXT.OpenConnection(address);
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine("Failed to connect: " + exception.ToString());
                        System.Threading.Thread.Sleep(1000);

                        connectionRetries++;
                        if (connectionRetries > maxRetries)
                        {
                            //  failed to connect
                            StatusDisconnected();
                            return false;
                        }

                        //  try again
                        StatusConnecting(string.Format("Connecting to {0}. Retry: {1}", ConnectionMethodString(address), connectionRetries.ToString()));

                        FrankenNXT.CloseConnection();
                        System.Threading.Thread.Sleep(250);
                    }
                }

                //  did we manage to connect
                if (FrankenNXT.IsConnected)
                {
                    //  connected
                    RobotLeftDriveMotor.IpAddress = address;
                    RobotRightDriveMotor.IpAddress = address;
                    RobotMotor2.IpAddress = address;
                    RobotMotor3.IpAddress = address;
                    StatusConnected();
                    return true;
                }
                else
                {
                    if (ConnectionStateChanged != null)
                        ConnectionStateChanged(this, new ConnectionEventArgs(ConnectionState.Disconnected));
                }
            }
            catch (Exception Exception)
            {
                StatusDisconnected();
            }

            return false;
        }

        #endregion  //  Connection


  

        //
        //  Command Queue
        //
        #region Command Queue Thread

        //  Command Queue
        public bool RunCommandQueueThread { get; protected set; }
        private Thread CommandQueueThread;

        /// <summary>
        /// Command queue thread run function
        /// </summary>
        public void CommandQueueThreadRun()
        {
            while (RunCommandQueueThread)
            {
                try
                {
                    //  is the command queue empty
                    if (CommandQueue.Count == 0)
                    {
                        NotifyData.WaitOne();
                    }

                    //  empty the command queue
                    List<RobotCommand> queue;
                    lock (LockCommandQueue)
                    {
                        queue = new List<RobotCommand>(CommandQueue);
                        CommandQueue.Clear();
                    }

                    //  process the command queue (remote redundant commands)
                    List<RobotCommand> filteredQueue = ProcessCommandQueue(queue);

                    foreach (var nextCommand in filteredQueue)
                    {
                        try
                        {
                            //  verify connection to the brick before sending next command
                            if ( FrankenNXT.IsConnected )
                                nextCommand.Command.Invoke();
                        }
                        catch (Exception e)
                        {

                        }
                    }
                }
                catch (Exception e)
                {
                    //  Catch thread run interrupt exception
                    if (!RunCommandQueueThread)
                        return;
                }

            }

        }

        #endregion  //  Status Thread



    }
}
