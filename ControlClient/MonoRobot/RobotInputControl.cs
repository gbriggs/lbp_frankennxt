﻿using System;
using System.Collections.Generic;
using MathNet.Spatial.Euclidean;

namespace MonoRobot
{
   
    //  Quadrant Enum
    //
    enum quadrant
    {
        NE,
        NW,
        SW,
        SE
    }

    //  Track driving direction
    //
    enum TrackDrivingDirection
    {
        Stop,
        Forward,
        Back
    }

    //  Joystick Direction
    enum JoystickDirection
    {
        Stop,
        Forward,
        ForwardRight,
        SpinRight,
        ReverseRight,
        Reverse,
        ReverseLeft,
        SpinLeft,
        ForwardLeft
    }

    //  Key Mapping to Robot control keypad
    //    qwe  789
    //    asd  456
    //    zxc  123
    //     vb   0.
    class Keymap
    {
        static public bool IsD2Key(ConsoleKey key)
        {
            return (key == ConsoleKey.D2 || key == ConsoleKey.NumPad2 || key == ConsoleKey.X);
        }
        static public bool IsD4Key(ConsoleKey key)
        {
            return (key == ConsoleKey.D4 || key == ConsoleKey.NumPad4 || key == ConsoleKey.A);
        }

        static public bool IsD6Key(ConsoleKey key)
        {
            return (key == ConsoleKey.D6 || key == ConsoleKey.NumPad6 || key == ConsoleKey.D);
        }

        static public bool IsD7Key(ConsoleKey key)
        {
            return (key == ConsoleKey.D7 || key == ConsoleKey.NumPad7 || key == ConsoleKey.Q);
        }

        static public bool IsD8Key(ConsoleKey key)
        {
            return (key == ConsoleKey.D8 || key == ConsoleKey.NumPad8 || key == ConsoleKey.W);
        }

        static public bool IsD9Key(ConsoleKey key)
        {
            return (key == ConsoleKey.D9 || key == ConsoleKey.NumPad9 || key == ConsoleKey.E);
        }

        static public bool IsD0Key(ConsoleKey key)
        {
            return (key == ConsoleKey.D0 || key == ConsoleKey.NumPad0 || key == ConsoleKey.V);
        }
    }

    // Robot Command
    // container for a command function to send to the robot
    class RobotCommand
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="commandObject">object that is issuing this command</param>
        /// <param name="command">delegate function</param>
        public RobotCommand(Object commandObject, Action command)
        {
            CommandObject = commandObject;
            Command = command;
        }

        public Object CommandObject;
        public Action Command;
    }



    //  Robot Input Control
    //  partial implementation of Robot class
    //  specifically to handle joystick and keyboard input control
    partial class Robot
    {

        /// <summary>
        /// Get Motor
        /// </summary>
        /// <param name="i">index of the motor</param>
        /// <returns>motor if it exists, otherwise null</returns>
        private Motor GetMotor(int i)
        {
            if (i < Motors.Count)
            {
                return Motors[i];
            }
            return null;
        }
        

        /// <summary>
        /// Process set motor command
        /// creates the command function to set motor and puts it in the processing queue
        /// </summary>
        /// <param name="motorIndex">index to the motor</param>
        /// <param name="speed">motor speed</param>
        void ProcessSetMotor(int motorIndex, int speed)
        {
            Motor motor = GetMotor(motorIndex);
            if (motor == null)
                return;

            RobotCommand cmd = new RobotCommand(motor, () =>
            {
                motor.On(speed);
                MotorStateChanged?.Invoke(this, new MotorEventArgs(true, speed, 0));
            });

            QueueCommand(cmd);
        }
        

        /// <summary>
        /// Process turn motor command
        /// creates the command function to turn motor and puts it in the processing queue
        /// </summary>
        /// <param name="motorIndex">index to the motor</param>
        /// <param name="degrees">degrees to turn</param>
        void ProcessTurnMotor(int motorIndex, int degrees, int speed)
        {
            Motor motor = GetMotor(motorIndex);
            if (motor == null)
                return;

            RobotCommand cmd = new RobotCommand(motor, () =>
            {
                motor.Move(speed, degrees); 

                //  todo event for move by turn to
                MotorStateChanged?.Invoke(this, new MotorEventArgs(true, degrees, 0));
            });

            QueueCommand(cmd);
        }
        

        /// <summary>
        /// Process motor off
        /// creates the command function to turn motor off and puts it in the processing queue
        /// </summary>
        /// <param name="motorIndex">index to the motor</param>
        /// <param name="resetTach">flag true if you want to reset the tach count</param>
        void ProcessMotorOff(int motorIndex, bool resetTach = false)
        {
            Motor motor = GetMotor(motorIndex);
            if (motor == null)
                return;
            RobotCommand cmd = new RobotCommand(motor, () =>
            {
                motor.Off();
               
                if (resetTach)
                    motor.ResetTacho();

                MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
            });

            QueueCommand(cmd);
        }



        //  Driving Inputs 
        //

        //  Joystick Control State
        //
        JoystickDirection LastJoystickDirection = JoystickDirection.Stop;
        Int32 LastJoystickSpeed = 0;
        Int32 LastArmSpeed = 0;
        Int32 LastClawSpeed = 0;
        Int32 LastJoystickTurnPercentage = 0;

        Int32 TrackDrivingSpeedLeft = 0;
        Int32 TrackDrivingSpeedRight = 0;

        //  Properties
        Motor RobotLeftDriveMotor = null;
        Motor RobotRightDriveMotor = null;
        Motor RobotMotor2 = null;
        Motor RobotMotor3 = null;






        /// <summary>
        /// Process XBox Controller Input - One implementation
        /// </summary>
        /// <param name="data"></param>
        protected void ProcessXboxControllerInputRightStick(SimpleJoy.XBoxJoystickEventArgs data)
        {
            if (FrankenNXT == null || !IsConnected)
                return;

            //  Right Button is driving motors kill switch
            if (data.Data.RightBumper)
            {
                QueueCommand(new RobotCommand(FrankenNXT.Vehicle, () =>
                {
                    FrankenNXT.Vehicle.Stop();
                }));

                LastJoystickSpeed = 0;
                LastJoystickTurnPercentage = 0;
                LastJoystickDirection = JoystickDirection.Stop;
            }
            else
            {
                double speedRaw = data.Data.RightStick.Length > .15 ? data.Data.RightStick.Length * 30.0 : 0.0;

                //  apply the throttle multiplier from the right trigger
                double speedThrottle = data.Data.RightTrigger * 70.0;
                if (speedRaw > 0.0)
                    speedRaw += speedThrottle;

                //  round this the the nearest five as an integer
                //  make sure it is no larger than 100
                Int32 speed = Math.Min(100, (Int32)Math.Round(speedRaw / 5.0) * 5);

                //  process the driving input
                ProcessDrivingInput(speed, data.Data.RightStick);
            }

            //  Handle inputs 
            if (data.Data.XBtn)
            {
                {
                    var cmd = new RobotCommand(GetMotor(1), () =>
                    {
                        GetMotor(1).On(100);
                        MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 100, 0));
                    });
                    QueueCommand(cmd);
                    cmd = null;
                }
            }


            if (data.Data.BBtn)
            {
                {
                    var cmd = new RobotCommand(GetMotor(1), () =>
                    {
                        GetMotor(1).Off();
                        MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                    });
                    QueueCommand(cmd);
                    cmd = null;
                }
            }


        }

        
        


        /// <summary>
        /// Process XBox Controller Joystick - Track Steering Implementation
        /// </summary>
        /// <param name="data"></param>
        void ProcessXboxControllerInputTrackSteering(SimpleJoy.XBoxJoystickEventArgs data)
        {
            if (data.Data.RightBumper)
            {
                QueueCommand(new RobotCommand(FrankenNXT.Vehicle, () =>
                {
                    FrankenNXT.Vehicle.Stop();
                }));

                LastJoystickSpeed = 0;
                LastJoystickTurnPercentage = 0;
                LastJoystickDirection = JoystickDirection.Stop;
            }
            else
            {
                //  Set the speed between with max of 30
                double speedLeftRaw = Math.Abs(data.Data.LeftStick.Y) > .15 ? data.Data.LeftStick.Y * 30.0 : 0.0;
                double speedRightRaw = Math.Abs(data.Data.RightStick.Y) > .15 ? data.Data.RightStick.Y * 30 : 0.0;

                //  apply the throttle multiplier from the right trigger
                double speedThrottle = data.Data.RightTrigger * 70.0;

                //  set the final speed
                if (speedLeftRaw > 0.0)
                    speedLeftRaw += speedThrottle;
                else if (speedLeftRaw < 0.0)
                    speedLeftRaw -= speedThrottle;

                if (speedRightRaw > 0.0)
                    speedRightRaw += speedThrottle;
                else if (speedRightRaw < 0.0)
                    speedRightRaw -= speedThrottle;

                //  round this the the nearest five as an integer
                Int32 speedLeft = Math.Min(100, (Int32)Math.Round(speedLeftRaw / 5.0) * 5);
                Int32 speedRight = Math.Min(100, (Int32)Math.Round(speedRightRaw / 5.0) * 5);

                if (FrankenNXT == null || !IsConnected)
                    return;

                //  send command to motor.
                if (speedLeft == 0 && speedRight == 0)
                {
                    if (TrackDrivingSpeedRight != 0 || TrackDrivingSpeedLeft != 0)
                    {
                        RobotCommand cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                        {
                            FrankenNXT.Vehicle.Stop();
                            TrackDrivingSpeedLeft = 0;
                            TrackDrivingSpeedRight = 0;
                        });
                        QueueCommand(cmd);

                    }
                }
                else if ((speedLeft != TrackDrivingSpeedLeft) || (speedRight != TrackDrivingSpeedRight))
                {
                    RobotCommand cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                    {
                    //  left track speed
                    if (Math.Abs(speedLeft - TrackDrivingSpeedLeft) > 10)
                        {
                            RobotLeftDriveMotor.On(speedLeft);
                            TrackDrivingSpeedLeft = speedLeft;
                            MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                        }

                        if (Math.Abs(speedRight - TrackDrivingSpeedRight) > 10)
                        {
                            RobotRightDriveMotor.On(speedRight);
                            TrackDrivingSpeedRight = speedRight;
                            MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                        }

                    });
                    QueueCommand(cmd);
                }
            }

            //  Handle inputs 
            if (data.Data.XBtn)
            {
                {
                    var cmd = new RobotCommand(GetMotor(1), () =>
                    {
                        GetMotor(1).On(100);
                        MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 100, 0));
                    });
                    QueueCommand(cmd);
                    cmd = null;
                }
            }


            if (data.Data.BBtn)
            {
                {
                    var cmd = new RobotCommand(GetMotor(1), () =>
                    {
                        GetMotor(1).Off();
                        MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                    });
                    QueueCommand(cmd);
                    cmd = null;
                }
            }

        }

 
        protected Vector2D VectorReferenceN = new Vector2D(0, 1);
        protected Vector2D VectorReferenceS = new Vector2D(0, -1);

        //  constant for one fifth of a quadrant
        static double FIFTHQUAD = ((Math.PI / 2.0) / 5.0);


        /// <summary>
        /// Joystick Driving Direction
        /// Take vector for joystick, and determine direction and turning percentage
        /// </summary>
        private void JoystickDrivingDirection(Vector2D joystick, out JoystickDirection direction, out int turningPercentage)
        {
            turningPercentage = 0;
            direction = JoystickDirection.Forward;

            //  determine the quadrant
            quadrant quad = quadrant.NE;
            //
            if (joystick.X >= 0 && joystick.Y >= 0)
                quad = quadrant.NE;
            else if (joystick.X < 0 && joystick.Y >= 0)
                quad = quadrant.NW;
            else if (joystick.X < 0 && joystick.Y < 0)
                quad = quadrant.SW;
            else
                quad = quadrant.SE;

            //  determine the angle
            double angle = 0.0;
            switch (quad)
            {
                case quadrant.NE:
                    case quadrant.NW:
                    angle = joystick.AngleTo(VectorReferenceN).Radians;
                    break;

                    case quadrant.SE:
                    case quadrant.SW:
                    angle = joystick.AngleTo(VectorReferenceS).Radians;
                    break;
            }

            //  Check the angle, range is first fifth = forward, next three fifths are turn, final fifth is spin
            if (angle < FIFTHQUAD)
            {
                //  straight ahead or behind in direction
                switch (quad)
                {
                    case quadrant.NE:
                        case quadrant.NW:
                        direction = JoystickDirection.Forward;
                        break;
                        case quadrant.SE:
                        case quadrant.SW:
                        direction = JoystickDirection.Reverse;
                        break;
                }
            }
            else if (angle >= FIFTHQUAD && angle < (4.0 * FIFTHQUAD))
            {
                //  turning, determine the turning percent
                if (angle >= FIFTHQUAD && angle < 2.0 * FIFTHQUAD)
                    turningPercentage = 20;
                else if (angle >= 2.0 * FIFTHQUAD  && angle < 3.0 * FIFTHQUAD)
                    turningPercentage = 60;
                else if (angle >= 3.0 * FIFTHQUAD)
                    turningPercentage = 80;

                //  forward turn or behind turn
                switch (quad)
                {
                    case quadrant.NE:
                        direction = JoystickDirection.ForwardRight;
                        break;
                        case quadrant.SE:
                        direction = JoystickDirection.ReverseRight;
                        break;
                        case quadrant.NW:
                        direction = JoystickDirection.ForwardLeft;
                        break;
                        case quadrant.SW:
                        direction = JoystickDirection.ReverseLeft;
                        break;
                }
            }
            else
            {
                //  forward turn or behind turn
                switch (quad)
                {
                    case quadrant.NE:
                        case quadrant.SE:
                        direction = JoystickDirection.SpinRight;

                        break;
                        case quadrant.NW:
                        case quadrant.SW:
                        direction = JoystickDirection.SpinLeft;
                        break;
                }
            }
        }


        /// <summary>
        /// Process driving input
        /// </summary>
        /// <param name="speed"></param>
        /// <param name="joystick"></param>
        protected void ProcessDrivingInput(int speed, Vector2D joystick)
        {
            //  if we are not moving fast enough to matter, just bail
            if (speed < 10)
            {
                if (LastJoystickSpeed == 0)
                    return;
                else
                {
                    QueueCommand(new RobotCommand(FrankenNXT.Vehicle, () =>
                    {
                        FrankenNXT.Vehicle.Stop();
                    }));

                    LastJoystickSpeed = 0;
                    LastJoystickTurnPercentage = 0;
                    LastJoystickDirection = JoystickDirection.Stop;
                }
            }
            else
            {
                int thisTurningPercentage = 0;
                JoystickDirection thisDirection = JoystickDirection.Stop;

                JoystickDrivingDirection(joystick, out thisDirection, out thisTurningPercentage);

                //  Did we make a change worth applying
                if (thisDirection != LastJoystickDirection ||
                    Math.Abs(speed - LastJoystickSpeed) > 10 ||
                    (IsTurning(thisDirection) && thisTurningPercentage != LastJoystickTurnPercentage))
                {
                   
                    if (FrankenNXT != null && IsConnected)
                    {
                        RobotCommand cmd = null;
                        //  set this item
                        switch (thisDirection)
                        {
                            case JoystickDirection.Forward:
                                cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                                {
                                    FrankenNXT.Vehicle.Forward(speed);
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;
                            case JoystickDirection.ForwardRight:
                                cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                                {
                                    FrankenNXT.Vehicle.TurnRightForward(speed, thisTurningPercentage);
                                    LastJoystickTurnPercentage = thisTurningPercentage;
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;
                            case JoystickDirection.SpinRight:
                                cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                                {
                                    FrankenNXT.Vehicle.SpinRight(speed);
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;

                            case JoystickDirection.ReverseRight:
                                cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                                {
                                    FrankenNXT.Vehicle.TurnRightReverse(speed, thisTurningPercentage);
                                    LastJoystickTurnPercentage = thisTurningPercentage;
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;

                            case JoystickDirection.Reverse:
                                cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                                {
                                    FrankenNXT.Vehicle.Reverse(speed);
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;

                            case JoystickDirection.ReverseLeft:
                                cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                                {
                                    FrankenNXT.Vehicle.TurnLeftReverse(speed, thisTurningPercentage);
                                    LastJoystickTurnPercentage = thisTurningPercentage;
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;

                            case JoystickDirection.SpinLeft:
                                cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                                {
                                    FrankenNXT.Vehicle.SpinLeft(speed);
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;

                            case JoystickDirection.ForwardLeft:
                                cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                                {
                                    FrankenNXT.Vehicle.TurnLeftForward(speed, thisTurningPercentage);
                                    LastJoystickTurnPercentage = thisTurningPercentage;
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                                break;
                        }


                        if (cmd != null)
                            QueueCommand(cmd);
                    }
                    LastJoystickSpeed = speed;
                    LastJoystickDirection = thisDirection;
                    LastJoystickTurnPercentage = IsTurning(LastJoystickDirection) ? thisTurningPercentage : 0;
                }           
            }
        }


        /// <summary>
        /// Is turning
        /// helper function to translate joystick direction into bool to indicate turning or straight motion
        /// </summary>
        /// <param name="dir"></param>
        /// <returns></returns>
        private bool IsTurning(JoystickDirection dir)
        {
            switch (dir)
            {
                case JoystickDirection.ForwardRight:
                case JoystickDirection.ForwardLeft:
                case JoystickDirection.ReverseRight:
                case JoystickDirection.ReverseLeft:
                    return true;

                default:
                    return false;
            }
        }



        //  Keyboard Input
        //
        bool ForwardDirection = false;
        int DrivingSpeed = 10;
        ConsoleKey lastKey = ConsoleKey.Spacebar;

    
        /// <summary>
        /// Process Keyboard Driving Input
        /// </summary>
        /// <param name="key">key pressed</param>
        void ProcessKeyboardDrivingInput(int key)
        {
            ConsoleKey keyValue = (ConsoleKey)key;
            RobotCommand cmd = null;

            switch (keyValue)
            {
             
                //  Driving - forward direction
                case ConsoleKey.D8:
                case ConsoleKey.W:
                case ConsoleKey.NumPad8:
                    {
                        //  speed up or slow down in the current direction
                        if (!ForwardDirection)
                        {
                            DrivingSpeed -= 10;
                            //  did we stop
                            if (DrivingSpeed == 0)
                            {
                                cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                                {
                                    FrankenNXT.Vehicle.Stop();
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });

                                ForwardDirection = true;
                                DrivingSpeed = 0;
                            }
                            else
                            {
                                cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                                {
                                    FrankenNXT.Vehicle.Reverse(DrivingSpeed);
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                            }
                        }
                        else
                        {
                            ForwardDirection = true;

                            if (Keymap.IsD8Key(lastKey) || DrivingSpeed == 0)
                                DrivingSpeed += 10;
                            DrivingSpeed = Math.Min(DrivingSpeed, 100);

                            cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                            {
                                FrankenNXT.Vehicle.Forward(DrivingSpeed);
                                MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                            });
                        }
                    }
                    break;

                //  Driving - reverse direction
                case ConsoleKey.D2:
                case ConsoleKey.X:
                case ConsoleKey.NumPad2:
                    {
                        //  speed up or slow down in the current direction
                        if (ForwardDirection && DrivingSpeed != 0)
                        {
                            DrivingSpeed -= 10;
                            //  did we stop
                            if (DrivingSpeed == 0)
                            {
                                cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                                {
                                    FrankenNXT.Vehicle.Stop();
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });

                                ForwardDirection = true;
                                DrivingSpeed = 0;
                            }
                            else
                            {
                                cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                                {
                                    FrankenNXT.Vehicle.Forward(DrivingSpeed);
                                    MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                                });
                            }
                        }
                        else
                        {
                            ForwardDirection = false;
                            if (Keymap.IsD2Key(lastKey) || DrivingSpeed == 0)
                                DrivingSpeed += 10;
                            DrivingSpeed = Math.Min(DrivingSpeed, 100);

                            cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                            {
                                FrankenNXT.Vehicle.Reverse(DrivingSpeed);
                                MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                            });
                        }
                    }
                    break;

                //  Driving left direction
                case ConsoleKey.D4:
                case ConsoleKey.A:
                case ConsoleKey.NumPad4:
                    {
                        if (Keymap.IsD4Key(lastKey) || DrivingSpeed == 0)
                            DrivingSpeed += 10;
                        DrivingSpeed = Math.Min(DrivingSpeed, 100);

                        if (ForwardDirection)
                        {
                            cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                            {
                                FrankenNXT.Vehicle.TurnLeftForward(DrivingSpeed, 50);
                                MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                            });
                        }
                        else
                        {
                            cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                            {
                                FrankenNXT.Vehicle.TurnRightReverse(DrivingSpeed, 50);
                                MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                            });

                            ForwardDirection = false;
                        }
                    }
                    break;

                //  Driving - right direction
                case ConsoleKey.D6:
                case ConsoleKey.D:
                case ConsoleKey.NumPad6:
                    {
                        if (Keymap.IsD6Key(lastKey) || DrivingSpeed == 0)
                            DrivingSpeed += 10;
                        DrivingSpeed = Math.Min(DrivingSpeed, 100);

                        if (ForwardDirection)
                        {
                            cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                            {
                                FrankenNXT.Vehicle.TurnRightForward(DrivingSpeed, 50);
                                MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                            });
                        }
                        else
                        {

                            cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                            {
                                FrankenNXT.Vehicle.TurnLeftReverse(DrivingSpeed, 50);
                                MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                            });

                            ForwardDirection = false;
                        }
                    }
                    break;

                //  Driving - spin left
                case ConsoleKey.D7:
                case ConsoleKey.Q:
                case ConsoleKey.NumPad7:
                    {
                        if (Keymap.IsD7Key(lastKey) || DrivingSpeed == 0)
                            DrivingSpeed += 10;
                        DrivingSpeed = Math.Min(DrivingSpeed, 100);

                        cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                        {
                            FrankenNXT.Vehicle.SpinLeft(DrivingSpeed);
                            MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                        });
                    }
                    break;

                //  Driving spin right
                case ConsoleKey.D9:
                case ConsoleKey.E:
                case ConsoleKey.NumPad9:
                    {
                        if (Keymap.IsD9Key(lastKey) || DrivingSpeed == 0)
                            DrivingSpeed += 10;
                        DrivingSpeed = Math.Min(DrivingSpeed, 100);

                        cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                        {
                            FrankenNXT.Vehicle.SpinRight(DrivingSpeed);
                            MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                        });
                    }
                    break;

              
                //  All motors off
                case ConsoleKey.D5:
                case ConsoleKey.S:
                case ConsoleKey.NumPad5:
                    {
                        cmd = new RobotCommand(FrankenNXT.Vehicle, () =>
                        {
                            FrankenNXT.Vehicle.Stop();
                            MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                        });
                        QueueCommand(cmd);
                        //  driving parameters init
                        ForwardDirection = true;
                        DrivingSpeed = 0;

                        

                        cmd = null;
                    }
                    break;

                //  Engine on
                case ConsoleKey.D1:
                case ConsoleKey.Z:
                case ConsoleKey.NumPad1:
                    {
                        cmd = new RobotCommand(GetMotor(1), () =>
                        {
                            GetMotor(1).On(100);
                            MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                        });
                        QueueCommand(cmd);
                        cmd = null;
                    }
                    break;

                //  Engine off
                case ConsoleKey.D3:
                case ConsoleKey.C:
                case ConsoleKey.NumPad3:
                    {
                        cmd = new RobotCommand(GetMotor(1), () =>
                        {
                            GetMotor(1).Off();
                            MotorStateChanged?.Invoke(this, new MotorEventArgs(true, 0, 0));
                        });
                        QueueCommand(cmd);
                        cmd = null;
                    }
                    break;
            }

            if (cmd != null)
                QueueCommand(cmd);

            lastKey = (ConsoleKey)key;
            return;

        }


        /// <summary>
        /// Process the command queue
        /// do this pre processing to remove redundant commands
        /// only one command from each object is allowed
        /// and motor control commands will cancel status update commands
        /// </summary>
        /// <param name="queue"></param>
        /// <returns>a filtered queue that is ready to send to the robot</returns>
        List<RobotCommand> ProcessCommandQueue(List<RobotCommand> queue)
        {
            List<RobotCommand> filteredQueue = new List<RobotCommand>();

            //  one vehicle command allowed
            Object vehicleCommand = null;
            //  one brick (status) command allowed
            Object brickCommand = null;

            foreach (var nextCommand in queue)
            {
                if (nextCommand.CommandObject == FrankenNXT.Vehicle)
                {
                    if (vehicleCommand == null)
                    {
                        vehicleCommand = FrankenNXT.Vehicle;
                        filteredQueue.Add(nextCommand);
                    }
                    else
                        continue;
                }
                else if (nextCommand.CommandObject.GetType() == typeof(Motor))
                {
                    //  process motors
                    //  reject this command if superceeded by another motor command or vehicle command
                    if (vehicleCommand != null && (nextCommand.CommandObject == RobotLeftDriveMotor || nextCommand.CommandObject == RobotRightDriveMotor))
                        continue;
                    else if (filteredQueue.Find(x => x.CommandObject == nextCommand.CommandObject) != null)
                        continue;
                    else
                    {
                        //  todo - find this motor and filter
                        filteredQueue.Add(nextCommand);
                    }
                }
                else if (nextCommand.CommandObject.GetType() == typeof(Sensor))
                {
                    //  process sensor command  
                    filteredQueue.Add(nextCommand);
                }
                else if (nextCommand.CommandObject.GetType() == typeof(FrankenNXTWrapper))
                {
                    if (brickCommand == null)
                        brickCommand = nextCommand.CommandObject;
                    else
                        continue;

                    // process brick command 
                    filteredQueue.Add(nextCommand);
                }
                else
                    filteredQueue.Add(nextCommand);
            }

            //  now dump status queries if we have pending motor commands
            if (vehicleCommand != null || filteredQueue.Find(x => x.CommandObject.GetType() == typeof(Motor)) != null)
                filteredQueue.RemoveAll(x => x.CommandObject.GetType() == typeof(FrankenNXTWrapper));

            return filteredQueue;
        }

    }
}

