﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoRobot
{
    class Motor
    {
        public Motor(string name)
        {
            Name = name;
        }


        public string Name { get; protected set; }
        public string IpAddress { get; set; }

        public void On(int speed)
        {
            var response = Tcpip.SendString($"$FN_MOTOR,{Name},{speed.ToString()}", IpAddress, FrankenNXTWrapper.RobotRcConnectionPort);
            Console.WriteLine(response);
        }

        public void Off()
        {
            var response = Tcpip.SendString($"$FN_MOTOR,{Name},0", IpAddress, FrankenNXTWrapper.RobotRcConnectionPort);
            Console.WriteLine(response);
        }

        public void Move(int speed, int degrees)
        {
            return;
        }

        public void ResetTacho()
        {

        }


        public int Speed { get; protected set; }
        public string ConnectedPort { get; protected set; }
        public int TachoCount { get; protected set; }
    }
}
