﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoRobot
{
    public enum ConnectionState
    {
        Unknown = 0,
        Disconnected,
        Connecting,
        Connected,
    }

    public enum ConnectMethod
    {
        Usb,
        Bluetooth,
        WiFi
    }

    //  Connection Events
    //
    public class ConnectionEventArgs : EventArgs
    {
        public ConnectionEventArgs(ConnectionState state, string message = "")
        {
            ConnectedState = state;
            Message = message;
        }

        public ConnectionState ConnectedState { get; protected set; }
        public string Message { get; protected set; }

    }
    public delegate void ConnectionEventHandler(object sender, ConnectionEventArgs e);


    //  Motor Events
    //
    public class MotorEventArgs : EventArgs
    {
       
        public MotorEventArgs(bool on, int speed, int tach)
        {
            On = on;
            Speed = speed;
            Tach = tach;
        }

        public bool On { get; protected set; }
        public int Speed { get; protected set; }
        public int Tach { get; protected set; }
    }
    public delegate void MotorEventHandler(object sender, MotorEventArgs e);


    //  Semsor Events
    //
    public class SensorEventArgs : EventArgs
    {

        public SensorEventArgs()
        {
            Reading = "";
        }

        public string Reading;
    }
    public delegate void SensorEventHandler(object sender, SensorEventArgs e);


    //  Status Update Events
    public delegate void StatusUpdateEventHandler(object sender, EventArgs e);
}
