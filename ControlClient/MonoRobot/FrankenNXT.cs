﻿

namespace MonoRobot
{
    class FrankenNXTWrapper
    {
        public static int RobotRcConnectionPort = 48889;
        public static int RobotClientConnectionPort = 48890;
        
        public FrankenNXTWrapper()
        {
            IsConnected = false;
            Vehicle = new VehicleWrapper();
        }
       
        public bool IsConnected { get; protected set; }

        public void OpenConnection(string address)
        {
            var response = Tcpip.SendString("$FN_CONNECT", address, RobotClientConnectionPort);
            if (response.Contains("$FN_CONNECT,ACK"))
            {
                Vehicle.IpAddress = address;
                _ConnectedIpAddress = address;
                IsConnected = true;
                System.Console.WriteLine(response);
            }
        }


        public void CloseConnection()
        {
            IsConnected = false;
        }

        public VehicleWrapper Vehicle { get; protected set; }

        protected string _ConnectedIpAddress { get; set; }
        public string ConnectedIpAddress
        {
            get
            {
                if (IsConnected)
                    return _ConnectedIpAddress;
                else
                    return "";
            }
        }
    }
}
