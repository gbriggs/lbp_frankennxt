﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoRobot
{
    public enum SensorMode
    {
        Unknown = 0,
        Vl53loxDistance,
        Accelerometer,
        Gyro,
        IMUPose
    }

    public class Sensor
    {
        public Sensor()
        {
            Mode = SensorMode.Unknown;
            Name = "";
        }

        public string Name { get; protected set; }
        public SensorMode Mode { get; protected set; }

        public string LatestReading { get { return "--"; } }
    }
}
