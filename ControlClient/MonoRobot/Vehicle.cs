﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MonoRobot
{
    class VehicleWrapper
    {
        public string IpAddress { get; set; }
       
        public void Stop()
        {
            Console.WriteLine("Vehicle Stop");
            var response = Tcpip.SendString("$FN_DRIVE,0,0", IpAddress, FrankenNXTWrapper.RobotRcConnectionPort);
            Console.WriteLine(response);
        }

        public void Forward(int speed)
        {
            Console.WriteLine($"Vehicle forward: {speed.ToString()}");
            var response = Tcpip.SendString($"$FN_DRIVE,{speed.ToString()},0", IpAddress, FrankenNXTWrapper.RobotRcConnectionPort);
            Console.WriteLine(response);
        }

        public void Reverse(int speed)
        {
            Console.WriteLine($"Vehicle Reverse: {speed.ToString()}");
            var response = Tcpip.SendString($"$FN_DRIVE,{(-1*speed).ToString()},0", IpAddress, FrankenNXTWrapper.RobotRcConnectionPort);
            Console.WriteLine(response);
        }

        public void TurnRightForward(int speed, int turnPercentage)
        {
            Console.WriteLine($"Vehicle Turn Right Forward: {speed.ToString()} {turnPercentage.ToString()}");
            var response = Tcpip.SendString($"$FN_DRIVE,{speed.ToString()},{turnPercentage.ToString()}", IpAddress, FrankenNXTWrapper.RobotRcConnectionPort);
            Console.WriteLine(response);

        }

        public void TurnRightReverse(int speed, int turnPercentage)
        {
            Console.WriteLine($"Vehicle Turn Right Reverse: {speed.ToString()} {turnPercentage.ToString()}");
            var response = Tcpip.SendString($"$FN_DRIVE,{(-1*speed).ToString()},{turnPercentage.ToString()}", IpAddress, FrankenNXTWrapper.RobotRcConnectionPort);
            Console.WriteLine(response);
        }

        public void SpinRight(int speed)
        {
            Console.WriteLine($"Vehicle Spin Right: {speed.ToString()}");
            var response = Tcpip.SendString($"$FN_DRIVE,{speed.ToString()},100", IpAddress, FrankenNXTWrapper.RobotRcConnectionPort);
            Console.WriteLine(response);
        }

        public void TurnLeftForward(int speed, int turnPercentage)
        {
            Console.WriteLine($"Vehicle Turn Left Forward: {speed.ToString()} {turnPercentage.ToString()}");
            var response = Tcpip.SendString($"$FN_DRIVE,{speed.ToString()},{(-1*turnPercentage).ToString()}", IpAddress, FrankenNXTWrapper.RobotRcConnectionPort);
            Console.WriteLine(response);
        }

        public void TurnLeftReverse(int speed, int turnPercentage)
        {
            Console.WriteLine($"Vehicle Turn Left Reverse: {speed.ToString()} {turnPercentage.ToString()}");
            var response = Tcpip.SendString($"$FN_DRIVE,{(-1 * speed).ToString()},{(-1*turnPercentage).ToString()}", IpAddress, FrankenNXTWrapper.RobotRcConnectionPort);
            Console.WriteLine(response);
        }

        public void SpinLeft(int speed)
        {
            Console.WriteLine($"Vehicle Spin Left: {speed.ToString()}");
            var response = Tcpip.SendString($"$FN_DRIVE,{speed.ToString()},-100", IpAddress, FrankenNXTWrapper.RobotRcConnectionPort);
            Console.WriteLine(response);
        }


    }
}
