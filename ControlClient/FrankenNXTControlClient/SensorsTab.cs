﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MonoRobot;

namespace FrankenNXTController
{
    public partial class SensorsTab : UserControl
    {
        public SensorsTab()
        {
            InitializeComponent();
        }

        //  List of the sensor controls
        List<SensorControl> SensorControls;

        //  Reference to the Robot
        protected Robot Robot { get; set; }

        public void InitSensorControl(Robot robot)
        {
            Robot = robot;

            SensorControls = new List<SensorControl>();
            sensorControl0.InitControl(Robot, 0, "Sensor 1");
            SensorControls.Add(sensorControl0);
            sensorControl1.InitControl(Robot, 1, "Sensor 2");
            SensorControls.Add(sensorControl1);
            sensorControl2.InitControl(Robot, 2, "Sensor 3");
            SensorControls.Add(sensorControl2);
            sensorControl3.InitControl(Robot, 3, "Sensor 4");
            SensorControls.Add(sensorControl3);
        }


        public void ReinitSensorControls()
        {
            //  go through each control and init
            foreach (var nextCtrl in SensorControls)
                nextCtrl.ReInitControl();
        }

        public void UpdateSensorControls()
        {
            foreach (var nextCtrl in SensorControls)
                nextCtrl.UpdateControl();
        }
    }
}
