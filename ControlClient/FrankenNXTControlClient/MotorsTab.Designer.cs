﻿namespace FrankenNXTController
{
    partial class MotorsTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxDrive = new System.Windows.Forms.GroupBox();
            this.labelJoystickMode = new System.Windows.Forms.Label();
            this.buttonThree = new System.Windows.Forms.Button();
            this.buttonOne = new System.Windows.Forms.Button();
            this.buttonSeven = new System.Windows.Forms.Button();
            this.buttonNine = new System.Windows.Forms.Button();
            this.checkBoxControlMotors = new System.Windows.Forms.CheckBox();
            this.buttonTwo = new System.Windows.Forms.Button();
            this.buttonEight = new System.Windows.Forms.Button();
            this.buttonSix = new System.Windows.Forms.Button();
            this.buttonFour = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.trackBarMotorA = new FrankenNXTController.TrackBarMotorControl();
            this.trackBarMotorB = new FrankenNXTController.TrackBarMotorControl();
            this.groupBoxDrive.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxDrive
            // 
            this.groupBoxDrive.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.groupBoxDrive.Controls.Add(this.labelJoystickMode);
            this.groupBoxDrive.Controls.Add(this.buttonThree);
            this.groupBoxDrive.Controls.Add(this.buttonOne);
            this.groupBoxDrive.Controls.Add(this.buttonSeven);
            this.groupBoxDrive.Controls.Add(this.buttonNine);
            this.groupBoxDrive.Controls.Add(this.checkBoxControlMotors);
            this.groupBoxDrive.Controls.Add(this.buttonTwo);
            this.groupBoxDrive.Controls.Add(this.buttonEight);
            this.groupBoxDrive.Controls.Add(this.buttonSix);
            this.groupBoxDrive.Controls.Add(this.buttonFour);
            this.groupBoxDrive.Controls.Add(this.buttonStop);
            this.groupBoxDrive.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxDrive.Location = new System.Drawing.Point(526, 5);
            this.groupBoxDrive.Name = "groupBoxDrive";
            this.groupBoxDrive.Size = new System.Drawing.Size(181, 289);
            this.groupBoxDrive.TabIndex = 28;
            this.groupBoxDrive.TabStop = false;
            this.groupBoxDrive.Text = "Drive Controls";
            // 
            // labelJoystickMode
            // 
            this.labelJoystickMode.AutoSize = true;
            this.labelJoystickMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelJoystickMode.Location = new System.Drawing.Point(14, 210);
            this.labelJoystickMode.Name = "labelJoystickMode";
            this.labelJoystickMode.Size = new System.Drawing.Size(63, 16);
            this.labelJoystickMode.TabIndex = 27;
            this.labelJoystickMode.Text = "Joystick: ";
            // 
            // buttonThree
            // 
            this.buttonThree.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThree.Location = new System.Drawing.Point(128, 143);
            this.buttonThree.Name = "buttonThree";
            this.buttonThree.Size = new System.Drawing.Size(33, 33);
            this.buttonThree.TabIndex = 22;
            this.buttonThree.Text = "3";
            this.buttonThree.UseVisualStyleBackColor = true;
            this.buttonThree.Click += new System.EventHandler(this.buttonThree_Click);
            // 
            // buttonOne
            // 
            this.buttonOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOne.Location = new System.Drawing.Point(34, 143);
            this.buttonOne.Name = "buttonOne";
            this.buttonOne.Size = new System.Drawing.Size(33, 33);
            this.buttonOne.TabIndex = 21;
            this.buttonOne.Text = "1";
            this.buttonOne.UseVisualStyleBackColor = true;
            this.buttonOne.Click += new System.EventHandler(this.buttonOne_Click);
            // 
            // buttonSeven
            // 
            this.buttonSeven.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSeven.Location = new System.Drawing.Point(42, 60);
            this.buttonSeven.Name = "buttonSeven";
            this.buttonSeven.Size = new System.Drawing.Size(33, 33);
            this.buttonSeven.TabIndex = 20;
            this.buttonSeven.Text = "7";
            this.buttonSeven.UseVisualStyleBackColor = true;
            this.buttonSeven.Click += new System.EventHandler(this.buttonSeven_Click);
            // 
            // buttonNine
            // 
            this.buttonNine.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNine.Location = new System.Drawing.Point(120, 60);
            this.buttonNine.Name = "buttonNine";
            this.buttonNine.Size = new System.Drawing.Size(33, 33);
            this.buttonNine.TabIndex = 19;
            this.buttonNine.Text = "9";
            this.buttonNine.UseVisualStyleBackColor = true;
            this.buttonNine.Click += new System.EventHandler(this.buttonNine_Click);
            // 
            // checkBoxControlMotors
            // 
            this.checkBoxControlMotors.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxControlMotors.Location = new System.Drawing.Point(14, 26);
            this.checkBoxControlMotors.Name = "checkBoxControlMotors";
            this.checkBoxControlMotors.Size = new System.Drawing.Size(161, 24);
            this.checkBoxControlMotors.TabIndex = 26;
            this.checkBoxControlMotors.Text = "Control Motors";
            this.checkBoxControlMotors.UseVisualStyleBackColor = true;
            this.checkBoxControlMotors.CheckedChanged += new System.EventHandler(this.checkBoxControlMotors_CheckedChanged);
            // 
            // buttonTwo
            // 
            this.buttonTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTwo.Location = new System.Drawing.Point(81, 138);
            this.buttonTwo.Name = "buttonTwo";
            this.buttonTwo.Size = new System.Drawing.Size(33, 33);
            this.buttonTwo.TabIndex = 16;
            this.buttonTwo.Text = "2";
            this.buttonTwo.UseVisualStyleBackColor = true;
            this.buttonTwo.Click += new System.EventHandler(this.buttonTwo_Click);
            // 
            // buttonEight
            // 
            this.buttonEight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEight.Location = new System.Drawing.Point(81, 60);
            this.buttonEight.Name = "buttonEight";
            this.buttonEight.Size = new System.Drawing.Size(33, 33);
            this.buttonEight.TabIndex = 13;
            this.buttonEight.Text = "8";
            this.buttonEight.UseVisualStyleBackColor = true;
            this.buttonEight.Click += new System.EventHandler(this.buttonEight_Click);
            // 
            // buttonSix
            // 
            this.buttonSix.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSix.Location = new System.Drawing.Point(120, 99);
            this.buttonSix.Name = "buttonSix";
            this.buttonSix.Size = new System.Drawing.Size(33, 33);
            this.buttonSix.TabIndex = 14;
            this.buttonSix.Text = "6";
            this.buttonSix.UseVisualStyleBackColor = true;
            this.buttonSix.Click += new System.EventHandler(this.buttonSix_Click);
            // 
            // buttonFour
            // 
            this.buttonFour.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFour.Location = new System.Drawing.Point(42, 99);
            this.buttonFour.Name = "buttonFour";
            this.buttonFour.Size = new System.Drawing.Size(33, 33);
            this.buttonFour.TabIndex = 15;
            this.buttonFour.Text = "4";
            this.buttonFour.UseVisualStyleBackColor = true;
            this.buttonFour.Click += new System.EventHandler(this.buttonFour_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStop.Location = new System.Drawing.Point(81, 99);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(33, 33);
            this.buttonStop.TabIndex = 17;
            this.buttonStop.Text = "5";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonFive_Click);
            // 
            // trackBarMotorA
            // 
            this.trackBarMotorA.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.trackBarMotorA.BackColor = System.Drawing.Color.Transparent;
            this.trackBarMotorA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trackBarMotorA.Location = new System.Drawing.Point(3, 5);
            this.trackBarMotorA.Name = "trackBarMotorA";
            this.trackBarMotorA.Size = new System.Drawing.Size(127, 325);
            this.trackBarMotorA.TabIndex = 33;
            // 
            // trackBarMotorB
            // 
            this.trackBarMotorB.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.trackBarMotorB.BackColor = System.Drawing.Color.Transparent;
            this.trackBarMotorB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.trackBarMotorB.Location = new System.Drawing.Point(133, 5);
            this.trackBarMotorB.Name = "trackBarMotorB";
            this.trackBarMotorB.Size = new System.Drawing.Size(127, 325);
            this.trackBarMotorB.TabIndex = 30;
            // 
            // MotorsTab
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.trackBarMotorA);
            this.Controls.Add(this.groupBoxDrive);
            this.Controls.Add(this.trackBarMotorB);
            this.Name = "MotorsTab";
            this.Size = new System.Drawing.Size(751, 333);
            this.Load += new System.EventHandler(this.MotorsTab_Load);
            this.groupBoxDrive.ResumeLayout(false);
            this.groupBoxDrive.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxDrive;
        private System.Windows.Forms.Button buttonThree;
        private System.Windows.Forms.Button buttonOne;
        private System.Windows.Forms.Button buttonSeven;
        private System.Windows.Forms.Button buttonNine;
        private System.Windows.Forms.Button buttonTwo;
        private System.Windows.Forms.Button buttonEight;
        private System.Windows.Forms.Button buttonSix;
        private System.Windows.Forms.Button buttonFour;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.CheckBox checkBoxControlMotors;
        private TrackBarMotorControl trackBarMotorB;
        private TrackBarMotorControl trackBarMotorA;
        private System.Windows.Forms.Label labelJoystickMode;
    }
}
