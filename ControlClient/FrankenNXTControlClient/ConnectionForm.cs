﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using MonoRobot;


namespace FrankenNXTController
{
    public partial class ConnectionForm : Form
    {
        public ConnectionForm()
        {
            InitializeComponent();

            //  TODO init combo box selection from prefs
           
        }

       

        public string Address
        {
            get
            {
                return comboBoxAddress.Text;

                
            }
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            Close();
        
        }

       
        

    }
}
