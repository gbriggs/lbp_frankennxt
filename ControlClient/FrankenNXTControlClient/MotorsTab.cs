﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SimpleJoy;
using PlatformHelper;


namespace FrankenNXTController
{
    public partial class MotorsTab : UserControl
    {
       
        /// <summary>
        /// Constructor
        /// </summary>
        public MotorsTab()
        {
            InitializeComponent();
           
            checkBoxControlMotors.Checked = false;
        
        }

        
        //  Controling Motors Checked
        public bool ControlMotors
        {
            get
            {
                return checkBoxControlMotors.Checked;
            }
        }

      
    
        //  List of track bar controls
        List<TrackBarMotorControl> MotorTrackBars;

        //  Reference to the robot
        protected MonoRobot.Robot Robot { get; set; }

        /// <summary>
        /// Init the page, setup track bar controls
        /// </summary>
        /// <param name="robot"></param>
        public void InitMotorControl(MonoRobot.Robot robot)
        {
            Robot = robot;

            MotorTrackBars = new List<TrackBarMotorControl>();
            trackBarMotorA.InitControl(Robot, 0, "LeftTrack");
            MotorTrackBars.Add(trackBarMotorA);
            trackBarMotorB.InitControl(Robot, 1, "RightTrack");
            MotorTrackBars.Add(trackBarMotorB);
          
            SetMotorPageUiState();
        }

       
        /// <summary>
        /// Set Page UI
        /// </summary>
        void SetMotorPageUiState()
        {
            foreach (var nextItem in MotorTrackBars)
                nextItem.EnableControl(checkBoxControlMotors.Checked);//checkBoxControlMotors.Checked && !checkBoxUseJoystick.Checked);


            buttonEight.Enabled = checkBoxControlMotors.Checked;//checkBoxControlMotors.Checked && !checkBoxUseJoystick.Checked;
            buttonSix.Enabled = checkBoxControlMotors.Checked;//checkBoxControlMotors.Checked && !checkBoxUseJoystick.Checked;
            buttonTwo.Enabled = checkBoxControlMotors.Checked;//checkBoxControlMotors.Checked && !checkBoxUseJoystick.Checked;
            buttonFour.Enabled = checkBoxControlMotors.Checked;//checkBoxControlMotors.Checked && !checkBoxUseJoystick.Checked;
            buttonStop.Enabled = checkBoxControlMotors.Checked;//checkBoxControlMotors.Checked && !checkBoxUseJoystick.Checked;
            buttonSeven.Enabled = checkBoxControlMotors.Checked;//checkBoxControlMotors.Checked && !checkBoxUseJoystick.Checked;
            buttonNine.Enabled = checkBoxControlMotors.Checked;//checkBoxControlMotors.Checked && !checkBoxUseJoystick.Checked;
            buttonOne.Enabled = checkBoxControlMotors.Checked;//checkBoxControlMotors.Checked && !checkBoxUseJoystick.Checked;
            buttonThree.Enabled = checkBoxControlMotors.Checked;//checkBoxControlMotors.Checked && !checkBoxUseJoystick.Checked;

           
        }

        public void SetJoystickModeDisplay()
        {
            switch ( Robot.XBoxJoystickUsageMode )
            {
                case MonoRobot.Robot.XboxJoystickMode.TrackSteer:
                    labelJoystickMode.Text = "Joystick: Track Steering";
                    break;
                case MonoRobot.Robot.XboxJoystickMode.RightStick:
                    labelJoystickMode.Text = "Joystick: Right Stick";
                    break;
                default:
                    labelJoystickMode.Text = " ";
                    break;
            }
        }

        public void SetJoystickDisconnectedDisplay()
        {
            labelJoystickMode.Text = "";
        }

        /// <summary>
        /// Update Motor Trackbar controls
        /// </summary>
        public void UpdateMotorTrackbarControls()
        {
            foreach (var nextTrackBar in MotorTrackBars)
            {
                nextTrackBar.UpdateControl();
            }
        }


        /// <summary>
        /// Check Use Motors handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxControlMotors_CheckedChanged(object sender, EventArgs e)
        {
            SetMotorPageUiState();
        }



        #region Drive Controls

        //  Forward
        private void buttonEight_Click(object sender, EventArgs e)
        {
            Robot.KeyboardDrivingInput((int)ConsoleKey.D8);
        }
        //  Right
        private void buttonSix_Click(object sender, EventArgs e)
        {
            Robot.KeyboardDrivingInput((int)ConsoleKey.D6);
        }
        //  Back
        private void buttonTwo_Click(object sender, EventArgs e)
        {
            Robot.KeyboardDrivingInput((int)ConsoleKey.D2);
        }
        //  Left
        private void buttonFour_Click(object sender, EventArgs e)
        {
            Robot.KeyboardDrivingInput((int)ConsoleKey.D4);
        }
        //  Stop
        private void buttonFive_Click(object sender, EventArgs e)
        {
            Robot.KeyboardDrivingInput((int)ConsoleKey.D5);
        }
        //  Spin Left
        private void buttonSeven_Click(object sender, EventArgs e)
        {
            Robot.KeyboardDrivingInput((int)ConsoleKey.D7);
        }
        //  Spin Right
        private void buttonNine_Click(object sender, EventArgs e)
        {
            Robot.KeyboardDrivingInput((int)ConsoleKey.D9);
        }
        //  Motor A Increase Speed
        private void buttonOne_Click(object sender, EventArgs e)
        {
            Robot.SetMotor(0, Robot.GetMotorSpeed(0) + 10);
        }
        //  Motor A Decrease Speed
        private void buttonThree_Click(object sender, EventArgs e)
        {
            Robot.SetMotor(0, Robot.GetMotorSpeed(0) - 10);
        }

        #endregion

        private void MotorsTab_Load(object sender, EventArgs e)
        {

        }

      
      

      
    }
}
