﻿namespace FrankenNXTController
{
    partial class SensorsTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sensorControl3 = new FrankenNXTController.SensorControl();
            this.sensorControl2 = new FrankenNXTController.SensorControl();
            this.sensorControl1 = new FrankenNXTController.SensorControl();
            this.sensorControl0 = new FrankenNXTController.SensorControl();
            this.SuspendLayout();
            // 
            // sensorControl3
            // 
            this.sensorControl3.Location = new System.Drawing.Point(3, 262);
            this.sensorControl3.Name = "sensorControl3";
            this.sensorControl3.Size = new System.Drawing.Size(745, 89);
            this.sensorControl3.TabIndex = 3;
            // 
            // sensorControl2
            // 
            this.sensorControl2.Location = new System.Drawing.Point(3, 173);
            this.sensorControl2.Name = "sensorControl2";
            this.sensorControl2.Size = new System.Drawing.Size(745, 89);
            this.sensorControl2.TabIndex = 2;
            // 
            // sensorControl1
            // 
            this.sensorControl1.Location = new System.Drawing.Point(3, 84);
            this.sensorControl1.Name = "sensorControl1";
            this.sensorControl1.Size = new System.Drawing.Size(745, 89);
            this.sensorControl1.TabIndex = 1;
            // 
            // sensorControl0
            // 
            this.sensorControl0.Location = new System.Drawing.Point(3, -5);
            this.sensorControl0.Name = "sensorControl0";
            this.sensorControl0.Size = new System.Drawing.Size(745, 89);
            this.sensorControl0.TabIndex = 0;
            // 
            // SensorsTab
            // 
            this.Controls.Add(this.sensorControl3);
            this.Controls.Add(this.sensorControl2);
            this.Controls.Add(this.sensorControl1);
            this.Controls.Add(this.sensorControl0);
            this.Name = "SensorsTab";
            this.Size = new System.Drawing.Size(751, 365);
            this.ResumeLayout(false);

        }

        #endregion

        private SensorControl sensorControl0;
        private SensorControl sensorControl1;
        private SensorControl sensorControl2;
        private SensorControl sensorControl3;

    }
}
