﻿using System;
using System.Windows.Forms;
using MonoRobot;

namespace FrankenNXTController
{
    public partial class SensorControl : UserControl
    {
        protected int SensorIndex { get; set; }
        protected Robot Robot { get; set; }

        public SensorControl()
        {
            InitializeComponent();
        }

        public void InitControl(Robot robot, int index, string label)
        {
            groupBoxSensor.Text = label;
            SensorIndex = index;
            Robot = robot;
        }

        public void ReInitControl()
        {
            //  get the sensor for this control
            labelName.Text = Robot.GetSensorName(SensorIndex);
           
            FillModeComboBox();

            //  set combo box settings
            comboBoxMode.SelectedItem = "";
        }

        public void UpdateLabels()
        {
            labelReading.Text = Robot.GetSensorLatestReading(SensorIndex);
        }

        public void SetComboBox(string value)
        {
            if (!ChoosingSensor)
            {
                foreach (var nextItem in comboBoxMode.Items)
                {
                    if (nextItem.ToString().CompareTo(value) == 0)
                    {
                        comboBoxMode.SelectedItem = nextItem;
                        break;
                    }
                }
            }
        }
        public void UpdateControl()
        {
            if ( Robot.IsConnected )
            {
                string modeName = "";

                SetComboBox(modeName);
            }

            UpdateLabels();
        }


        public void EnableControl(bool enable)
        {
            comboBoxMode.Enabled = enable;
        }

        protected void FillModeComboBox()
        {
            comboBoxMode.Items.Clear();

            
        }

        private void comboBoxMode_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBoxMode.SelectedItem != null)
            {
                ComboBox cb = (ComboBox)sender;

                
            }
        }

        bool ChoosingSensor = false;

        private void comboBoxMode_DropDown(object sender, EventArgs e)
        {
            ChoosingSensor = true;
        }

        private void comboBoxMode_DrawItem(object sender, DrawItemEventArgs e)
        {
            ChoosingSensor = false;
        }
    }
}
