﻿namespace FrankenNXTController
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorkerConnection = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerDisconnect = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorkerConnectJoystick = new System.ComponentModel.BackgroundWorker();
            this.tabPageGpio = new System.Windows.Forms.TabPage();
            this.tabPageSensors = new System.Windows.Forms.TabPage();
            this.sensorsTab = new FrankenNXTController.SensorsTab();
            this.tabPageMotors = new System.Windows.Forms.TabPage();
            this.motorsTab = new FrankenNXTController.MotorsTab();
            this.tabPageRobot = new System.Windows.Forms.TabPage();
            this.groupBoxSensors = new System.Windows.Forms.GroupBox();
            this.checkBoxMonitorGPS = new System.Windows.Forms.CheckBox();
            this.checkBoxMonitorIMU = new System.Windows.Forms.CheckBox();
            this.labelSensorStatus = new System.Windows.Forms.Label();
            this.checkBoxMonitorDistance = new System.Windows.Forms.CheckBox();
            this.labelConnection = new System.Windows.Forms.Label();
            this.groupBoxMotors = new System.Windows.Forms.GroupBox();
            this.checkBoxMonitorMotorsTach = new System.Windows.Forms.CheckBox();
            this.labelMotorStatus = new System.Windows.Forms.Label();
            this.checkBoxMonitorMotorsSpeed = new System.Windows.Forms.CheckBox();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.groupBoxJoystick = new System.Windows.Forms.GroupBox();
            this.buttonRefresh = new System.Windows.Forms.Button();
            this.buttonConnectJoystick = new System.Windows.Forms.Button();
            this.comboBoxJoystickPaths = new System.Windows.Forms.ComboBox();
            this.tabControlRobot = new System.Windows.Forms.TabControl();
            this.tabPageSensors.SuspendLayout();
            this.tabPageMotors.SuspendLayout();
            this.tabPageRobot.SuspendLayout();
            this.groupBoxSensors.SuspendLayout();
            this.groupBoxMotors.SuspendLayout();
            this.groupBoxJoystick.SuspendLayout();
            this.tabControlRobot.SuspendLayout();
            this.SuspendLayout();
            // 
            // backgroundWorkerConnection
            // 
            this.backgroundWorkerConnection.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerConnection_DoWork);
            this.backgroundWorkerConnection.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerConnection_RunWorkerCompleted);
            // 
            // backgroundWorkerDisconnect
            // 
            this.backgroundWorkerDisconnect.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerDisconnection_DoWork);
            // 
            // backgroundWorkerConnectJoystick
            // 
            this.backgroundWorkerConnectJoystick.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorkerConnectJoystick_DoWork);
            this.backgroundWorkerConnectJoystick.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorkerConnectJoystick_RunWorkerCompleted);
            // 
            // tabPageGpio
            // 
            this.tabPageGpio.Location = new System.Drawing.Point(4, 24);
            this.tabPageGpio.Name = "tabPageGpio";
            this.tabPageGpio.Size = new System.Drawing.Size(752, 367);
            this.tabPageGpio.TabIndex = 4;
            // 
            // tabPageSensors
            // 
            this.tabPageSensors.Controls.Add(this.sensorsTab);
            this.tabPageSensors.Location = new System.Drawing.Point(4, 29);
            this.tabPageSensors.Name = "tabPageSensors";
            this.tabPageSensors.Size = new System.Drawing.Size(752, 362);
            this.tabPageSensors.TabIndex = 2;
            this.tabPageSensors.Text = "Sensors";
            this.tabPageSensors.UseVisualStyleBackColor = true;
            // 
            // sensorsTab
            // 
            this.sensorsTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sensorsTab.Location = new System.Drawing.Point(4, 5);
            this.sensorsTab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.sensorsTab.Name = "sensorsTab";
            this.sensorsTab.Size = new System.Drawing.Size(744, 371);
            this.sensorsTab.TabIndex = 0;
            // 
            // tabPageMotors
            // 
            this.tabPageMotors.Controls.Add(this.motorsTab);
            this.tabPageMotors.Location = new System.Drawing.Point(4, 29);
            this.tabPageMotors.Name = "tabPageMotors";
            this.tabPageMotors.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMotors.Size = new System.Drawing.Size(752, 362);
            this.tabPageMotors.TabIndex = 1;
            this.tabPageMotors.Text = "Motors";
            this.tabPageMotors.UseVisualStyleBackColor = true;
            // 
            // motorsTab
            // 
            this.motorsTab.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.motorsTab.BackColor = System.Drawing.Color.Transparent;
            this.motorsTab.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.motorsTab.Location = new System.Drawing.Point(4, 8);
            this.motorsTab.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.motorsTab.Name = "motorsTab";
            this.motorsTab.Size = new System.Drawing.Size(752, 349);
            this.motorsTab.TabIndex = 0;
            // 
            // tabPageRobot
            // 
            this.tabPageRobot.Controls.Add(this.groupBoxSensors);
            this.tabPageRobot.Controls.Add(this.labelConnection);
            this.tabPageRobot.Controls.Add(this.groupBoxMotors);
            this.tabPageRobot.Controls.Add(this.buttonConnect);
            this.tabPageRobot.Controls.Add(this.groupBoxJoystick);
            this.tabPageRobot.Location = new System.Drawing.Point(4, 29);
            this.tabPageRobot.Name = "tabPageRobot";
            this.tabPageRobot.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageRobot.Size = new System.Drawing.Size(752, 362);
            this.tabPageRobot.TabIndex = 0;
            this.tabPageRobot.Text = "Home";
            this.tabPageRobot.UseVisualStyleBackColor = true;
            // 
            // groupBoxSensors
            // 
            this.groupBoxSensors.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSensors.Controls.Add(this.checkBoxMonitorGPS);
            this.groupBoxSensors.Controls.Add(this.checkBoxMonitorIMU);
            this.groupBoxSensors.Controls.Add(this.labelSensorStatus);
            this.groupBoxSensors.Controls.Add(this.checkBoxMonitorDistance);
            this.groupBoxSensors.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxSensors.Location = new System.Drawing.Point(340, 44);
            this.groupBoxSensors.Name = "groupBoxSensors";
            this.groupBoxSensors.Size = new System.Drawing.Size(270, 211);
            this.groupBoxSensors.TabIndex = 2;
            this.groupBoxSensors.TabStop = false;
            this.groupBoxSensors.Text = "Sensors";
            // 
            // checkBoxMonitorGPS
            // 
            this.checkBoxMonitorGPS.AutoSize = true;
            this.checkBoxMonitorGPS.Location = new System.Drawing.Point(16, 77);
            this.checkBoxMonitorGPS.Name = "checkBoxMonitorGPS";
            this.checkBoxMonitorGPS.Size = new System.Drawing.Size(119, 24);
            this.checkBoxMonitorGPS.TabIndex = 6;
            this.checkBoxMonitorGPS.Text = "Monitor GPS";
            this.checkBoxMonitorGPS.UseVisualStyleBackColor = true;
            this.checkBoxMonitorGPS.CheckedChanged += new System.EventHandler(this.checkBoxMonitorGPS_CheckedChanged);
            // 
            // checkBoxMonitorIMU
            // 
            this.checkBoxMonitorIMU.AutoSize = true;
            this.checkBoxMonitorIMU.Location = new System.Drawing.Point(16, 52);
            this.checkBoxMonitorIMU.Name = "checkBoxMonitorIMU";
            this.checkBoxMonitorIMU.Size = new System.Drawing.Size(115, 24);
            this.checkBoxMonitorIMU.TabIndex = 5;
            this.checkBoxMonitorIMU.Text = "Monitor IMU";
            this.checkBoxMonitorIMU.UseVisualStyleBackColor = true;
            this.checkBoxMonitorIMU.CheckedChanged += new System.EventHandler(this.checkBoxMonitorIMU_CheckedChanged);
            // 
            // labelSensorStatus
            // 
            this.labelSensorStatus.AutoSize = true;
            this.labelSensorStatus.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSensorStatus.Location = new System.Drawing.Point(13, 104);
            this.labelSensorStatus.Name = "labelSensorStatus";
            this.labelSensorStatus.Size = new System.Drawing.Size(144, 16);
            this.labelSensorStatus.TabIndex = 2;
            this.labelSensorStatus.Text = "labelSensorStatus";
            // 
            // checkBoxMonitorDistance
            // 
            this.checkBoxMonitorDistance.AutoSize = true;
            this.checkBoxMonitorDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMonitorDistance.Location = new System.Drawing.Point(16, 25);
            this.checkBoxMonitorDistance.Name = "checkBoxMonitorDistance";
            this.checkBoxMonitorDistance.Size = new System.Drawing.Size(190, 24);
            this.checkBoxMonitorDistance.TabIndex = 1;
            this.checkBoxMonitorDistance.Text = "Monitor Front Distance";
            this.checkBoxMonitorDistance.UseVisualStyleBackColor = true;
            this.checkBoxMonitorDistance.CheckedChanged += new System.EventHandler(this.checkBoxMonitorDistance_CheckedChanged);
            // 
            // labelConnection
            // 
            this.labelConnection.AutoSize = true;
            this.labelConnection.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelConnection.Location = new System.Drawing.Point(118, 14);
            this.labelConnection.Name = "labelConnection";
            this.labelConnection.Size = new System.Drawing.Size(123, 20);
            this.labelConnection.TabIndex = 1;
            this.labelConnection.Text = "labelConnection";
            // 
            // groupBoxMotors
            // 
            this.groupBoxMotors.Controls.Add(this.checkBoxMonitorMotorsTach);
            this.groupBoxMotors.Controls.Add(this.labelMotorStatus);
            this.groupBoxMotors.Controls.Add(this.checkBoxMonitorMotorsSpeed);
            this.groupBoxMotors.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxMotors.Location = new System.Drawing.Point(6, 44);
            this.groupBoxMotors.Name = "groupBoxMotors";
            this.groupBoxMotors.Size = new System.Drawing.Size(316, 145);
            this.groupBoxMotors.TabIndex = 1;
            this.groupBoxMotors.TabStop = false;
            this.groupBoxMotors.Text = "Motors";
            // 
            // checkBoxMonitorMotorsTach
            // 
            this.checkBoxMonitorMotorsTach.AutoSize = true;
            this.checkBoxMonitorMotorsTach.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMonitorMotorsTach.Location = new System.Drawing.Point(7, 52);
            this.checkBoxMonitorMotorsTach.Name = "checkBoxMonitorMotorsTach";
            this.checkBoxMonitorMotorsTach.Size = new System.Drawing.Size(120, 24);
            this.checkBoxMonitorMotorsTach.TabIndex = 2;
            this.checkBoxMonitorMotorsTach.Text = "Monitor Tach";
            this.checkBoxMonitorMotorsTach.UseVisualStyleBackColor = true;
            this.checkBoxMonitorMotorsTach.CheckedChanged += new System.EventHandler(this.checkBoxMonitorMotorsTach_CheckedChanged);
            // 
            // labelMotorStatus
            // 
            this.labelMotorStatus.AutoSize = true;
            this.labelMotorStatus.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMotorStatus.Location = new System.Drawing.Point(4, 85);
            this.labelMotorStatus.Name = "labelMotorStatus";
            this.labelMotorStatus.Size = new System.Drawing.Size(136, 16);
            this.labelMotorStatus.TabIndex = 1;
            this.labelMotorStatus.Text = "labelMotorStatus";
            // 
            // checkBoxMonitorMotorsSpeed
            // 
            this.checkBoxMonitorMotorsSpeed.AutoSize = true;
            this.checkBoxMonitorMotorsSpeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxMonitorMotorsSpeed.Location = new System.Drawing.Point(7, 26);
            this.checkBoxMonitorMotorsSpeed.Name = "checkBoxMonitorMotorsSpeed";
            this.checkBoxMonitorMotorsSpeed.Size = new System.Drawing.Size(132, 24);
            this.checkBoxMonitorMotorsSpeed.TabIndex = 0;
            this.checkBoxMonitorMotorsSpeed.Text = "Monitor Speed";
            this.checkBoxMonitorMotorsSpeed.UseVisualStyleBackColor = true;
            this.checkBoxMonitorMotorsSpeed.CheckedChanged += new System.EventHandler(this.checkBoxMonitorMotors_CheckedChanged);
            // 
            // buttonConnect
            // 
            this.buttonConnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConnect.Location = new System.Drawing.Point(6, 10);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(106, 28);
            this.buttonConnect.TabIndex = 0;
            this.buttonConnect.Text = "button1";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // groupBoxJoystick
            // 
            this.groupBoxJoystick.Controls.Add(this.buttonRefresh);
            this.groupBoxJoystick.Controls.Add(this.buttonConnectJoystick);
            this.groupBoxJoystick.Controls.Add(this.comboBoxJoystickPaths);
            this.groupBoxJoystick.Location = new System.Drawing.Point(6, 200);
            this.groupBoxJoystick.Name = "groupBoxJoystick";
            this.groupBoxJoystick.Size = new System.Drawing.Size(322, 60);
            this.groupBoxJoystick.TabIndex = 25;
            this.groupBoxJoystick.TabStop = false;
            this.groupBoxJoystick.Text = "Joystick";
            // 
            // buttonRefresh
            // 
            this.buttonRefresh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRefresh.Location = new System.Drawing.Point(200, 30);
            this.buttonRefresh.Name = "buttonRefresh";
            this.buttonRefresh.Size = new System.Drawing.Size(77, 24);
            this.buttonRefresh.TabIndex = 19;
            this.buttonRefresh.Text = "Refresh";
            this.buttonRefresh.UseVisualStyleBackColor = true;
            this.buttonRefresh.Click += new System.EventHandler(this.buttonRefresh_Click);
            // 
            // buttonConnectJoystick
            // 
            this.buttonConnectJoystick.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonConnectJoystick.Location = new System.Drawing.Point(8, 29);
            this.buttonConnectJoystick.Name = "buttonConnectJoystick";
            this.buttonConnectJoystick.Size = new System.Drawing.Size(102, 24);
            this.buttonConnectJoystick.TabIndex = 0;
            this.buttonConnectJoystick.Text = "Connect";
            this.buttonConnectJoystick.UseVisualStyleBackColor = true;
            this.buttonConnectJoystick.Click += new System.EventHandler(this.buttonConnectJoystick_Click);
            // 
            // comboBoxJoystickPaths
            // 
            this.comboBoxJoystickPaths.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxJoystickPaths.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxJoystickPaths.FormattingEnabled = true;
            this.comboBoxJoystickPaths.Location = new System.Drawing.Point(116, 30);
            this.comboBoxJoystickPaths.Name = "comboBoxJoystickPaths";
            this.comboBoxJoystickPaths.Size = new System.Drawing.Size(78, 24);
            this.comboBoxJoystickPaths.TabIndex = 18;
            // 
            // tabControlRobot
            // 
            this.tabControlRobot.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControlRobot.Controls.Add(this.tabPageRobot);
            this.tabControlRobot.Controls.Add(this.tabPageMotors);
            this.tabControlRobot.Controls.Add(this.tabPageSensors);
            this.tabControlRobot.Controls.Add(this.tabPageGpio);
            this.tabControlRobot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControlRobot.Location = new System.Drawing.Point(12, 12);
            this.tabControlRobot.Name = "tabControlRobot";
            this.tabControlRobot.SelectedIndex = 0;
            this.tabControlRobot.Size = new System.Drawing.Size(760, 395);
            this.tabControlRobot.TabIndex = 2;
            // 
            // MainForm
            // 
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(784, 419);
            this.Controls.Add(this.tabControlRobot);
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Text = "FrankenNXT Controller";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainForm_KeyDown);
            this.tabPageSensors.ResumeLayout(false);
            this.tabPageMotors.ResumeLayout(false);
            this.tabPageRobot.ResumeLayout(false);
            this.tabPageRobot.PerformLayout();
            this.groupBoxSensors.ResumeLayout(false);
            this.groupBoxSensors.PerformLayout();
            this.groupBoxMotors.ResumeLayout(false);
            this.groupBoxMotors.PerformLayout();
            this.groupBoxJoystick.ResumeLayout(false);
            this.tabControlRobot.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.ComponentModel.BackgroundWorker backgroundWorkerConnection;
        private System.ComponentModel.BackgroundWorker backgroundWorkerDisconnect;
        private System.ComponentModel.BackgroundWorker backgroundWorkerConnectJoystick;
        private System.Windows.Forms.TabPage tabPageGpio;
        private System.Windows.Forms.TabPage tabPageSensors;
        private SensorsTab sensorsTab;
        private System.Windows.Forms.TabPage tabPageMotors;
        private MotorsTab motorsTab;
        private System.Windows.Forms.TabPage tabPageRobot;
        private System.Windows.Forms.GroupBox groupBoxSensors;
        private System.Windows.Forms.Label labelSensorStatus;
        private System.Windows.Forms.Label labelConnection;
        private System.Windows.Forms.GroupBox groupBoxMotors;
        private System.Windows.Forms.CheckBox checkBoxMonitorMotorsTach;
        private System.Windows.Forms.Label labelMotorStatus;
        private System.Windows.Forms.CheckBox checkBoxMonitorMotorsSpeed;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.GroupBox groupBoxJoystick;
        private System.Windows.Forms.Button buttonRefresh;
        private System.Windows.Forms.Button buttonConnectJoystick;
        private System.Windows.Forms.ComboBox comboBoxJoystickPaths;
        private System.Windows.Forms.TabControl tabControlRobot;
        private System.Windows.Forms.CheckBox checkBoxMonitorGPS;
        private System.Windows.Forms.CheckBox checkBoxMonitorIMU;
        private System.Windows.Forms.CheckBox checkBoxMonitorDistance;
    }
}