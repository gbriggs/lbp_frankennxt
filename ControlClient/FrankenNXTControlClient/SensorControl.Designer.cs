﻿namespace FrankenNXTController
{
    partial class SensorControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxSensor = new System.Windows.Forms.GroupBox();
            this.labelSetMode = new System.Windows.Forms.Label();
            this.comboBoxMode = new System.Windows.Forms.ComboBox();
            this.labelReading = new System.Windows.Forms.Label();
            this.labelMode = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.groupBoxSensor.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxSensor
            // 
            this.groupBoxSensor.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxSensor.Controls.Add(this.labelSetMode);
            this.groupBoxSensor.Controls.Add(this.comboBoxMode);
            this.groupBoxSensor.Controls.Add(this.labelReading);
            this.groupBoxSensor.Controls.Add(this.labelMode);
            this.groupBoxSensor.Controls.Add(this.labelName);
            this.groupBoxSensor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxSensor.Location = new System.Drawing.Point(4, 4);
            this.groupBoxSensor.Name = "groupBoxSensor";
            this.groupBoxSensor.Size = new System.Drawing.Size(732, 75);
            this.groupBoxSensor.TabIndex = 0;
            this.groupBoxSensor.TabStop = false;
            this.groupBoxSensor.Text = "groupBoxSensor";
            // 
            // labelSetMode
            // 
            this.labelSetMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSetMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSetMode.Location = new System.Drawing.Point(479, 23);
            this.labelSetMode.Name = "labelSetMode";
            this.labelSetMode.Size = new System.Drawing.Size(74, 22);
            this.labelSetMode.TabIndex = 5;
            this.labelSetMode.Text = "Set Mode:";
            // 
            // comboBoxMode
            // 
            this.comboBoxMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxMode.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMode.FormattingEnabled = true;
            this.comboBoxMode.Location = new System.Drawing.Point(557, 19);
            this.comboBoxMode.Name = "comboBoxMode";
            this.comboBoxMode.Size = new System.Drawing.Size(169, 28);
            this.comboBoxMode.TabIndex = 4;
            this.comboBoxMode.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.comboBoxMode_DrawItem);
            this.comboBoxMode.DropDown += new System.EventHandler(this.comboBoxMode_DropDown);
            this.comboBoxMode.SelectionChangeCommitted += new System.EventHandler(this.comboBoxMode_SelectionChangeCommitted);
            // 
            // labelReading
            // 
            this.labelReading.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelReading.AutoSize = true;
            this.labelReading.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReading.Location = new System.Drawing.Point(10, 47);
            this.labelReading.Name = "labelReading";
            this.labelReading.Size = new System.Drawing.Size(118, 18);
            this.labelReading.TabIndex = 2;
            this.labelReading.Text = "- reading -";
            // 
            // labelMode
            // 
            this.labelMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelMode.AutoSize = true;
            this.labelMode.Location = new System.Drawing.Point(335, 22);
            this.labelMode.Name = "labelMode";
            this.labelMode.Size = new System.Drawing.Size(53, 20);
            this.labelMode.TabIndex = 1;
            this.labelMode.Text = "Mode:";
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Location = new System.Drawing.Point(7, 22);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(64, 20);
            this.labelName.TabIndex = 0;
            this.labelName.Text = "Sensor:";
            // 
            // SensorControl
            // 
            this.Controls.Add(this.groupBoxSensor);
            this.Name = "SensorControl";
            this.Size = new System.Drawing.Size(739, 82);
            this.groupBoxSensor.ResumeLayout(false);
            this.groupBoxSensor.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxSensor;
        private System.Windows.Forms.ComboBox comboBoxMode;
        private System.Windows.Forms.Label labelReading;
        private System.Windows.Forms.Label labelMode;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelSetMode;
    }
}
