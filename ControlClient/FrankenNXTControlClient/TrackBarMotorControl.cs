﻿using System;
using System.Windows.Forms;
using MonoRobot;


namespace FrankenNXTController
{
    public partial class TrackBarMotorControl : UserControl
    {
        protected int MotorIndex { get; set; }

        protected MonoRobot.Robot Robot { get; set; }

        public TrackBarMotorControl()
        {
            InitializeComponent();
        }

        public void InitControl(Robot robot, int index, string label)
        {
            groupBoxMotor.Text = label;
            MotorIndex = index;
            Robot = robot;
        }

        public void UpdateLabels()
        {
            string speedLabel = "";
            string tachLabel = "";

            if (Robot.IsConnected)
            {
                speedLabel = "Speed: " + Robot.GetMotorSpeed(MotorIndex).ToString();
                tachLabel = "Tach: " + Robot.GetMotorTachoCount(MotorIndex).ToString();
            }

            labelMotorSpeed.Text = speedLabel;
            labelMotorTach.Text = tachLabel;
        }



        public void UpdateControl()
        {
            //trackBarMotor.Value = Math.Min(Robot.GetMotorSpeed(MotorIndex), 100) / 5;
            UpdateLabels();
        }


        public void EnableControl(bool enable)
        {
            Enabled = enable;
            labelMotorSpeed.Enabled = enable;
            labelMotorTach.Enabled = enable;
        }

        private void trackBarMotor_Scroll(object sender, EventArgs e)
        {

            try
            {
                Enabled = false;

                if (!Robot.IsConnected)
                {
                    trackBarMotor.Value = 0;
                    return;
                }

                try
                {
                    Robot.SetMotor(MotorIndex, trackBarMotor.Value * 5);
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Failed to send motor command " + exception.ToString());
                }
            }
            finally
            {
                UpdateLabels();
                Enabled = true;
            }
        }
        private void buttonMotorStop_Click(object sender, EventArgs e)
        {
            try
            {
                buttonMotorAStop.Enabled = false;
                if (!Robot.IsConnected)
                    return;

                try
                {
                    Robot.MotorOff(MotorIndex, true);
                    trackBarMotor.Value = 0;
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Failed to send motor command " + exception.ToString());
                }
            }
            finally
            {
                UpdateControl();
                buttonMotorAStop.Enabled = true;
            }
        }

    }
}
