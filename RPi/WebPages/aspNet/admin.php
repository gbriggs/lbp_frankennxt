<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
          "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <title>frankenNXT</title>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="../style.css" type="text/css" />
    <!--[if IE 6]>
    <link rel="stylesheet" href="fix.css" type="text/css" />
    <![endif]-->
</head>
<body>
   <div id="sidebar">
        <div id="logo">  
                <img src="./Images/frankenNXT-logo.png" alt="franenNXT"  />
            </a>
        </div>
        <h2>frankenNXT</h2>

        <div id="menu">
            <a  href="index.html">Admin</a>
            <a href="./WiFi.aspx">Wi-Fi Settings</a>
            <a href="performance.php">Processes</a>
            <a href="sysinfo.php">Compilers</a>
            <a href="piinfo.php">System</a>
            <a class="active" href="admin.php">Reboot / Shutdown</a>
        </div>
    </div>
    <div id="content">


    <h2>Reboot / Shutdown</h2>
  
<input type="button" id="reboot"  value="Reboot" onClick="reboot()"/> 
<input type="button" id="reboot"  value="Shut Down" onClick="shutdown()"/> 
 

<script>  
  function reboot() {  
       window.location="index.php?status=reboot";  
  }  
  function shutdown() {  
      window.location="index.php?status=shutdown";  
 }  
  
</script>  
   
   
   <?php  
   $status = $_GET["status"];  
  
  if ($status == "reboot")
  {  
       exec("python /usr/share/nginx/www/admin/reboot.py");
       print '<h2>Server is rebooting ...</h2>';  
  }  
  else if ($status == "shutdown")
  {
  		exec("python /usr/share/nginx/www/admin/shutdown.py");
  		print '<h2>Server is shutting down ...</h2>';
  }
  
?>  
 
        </div>
    </div></body>
</html>

