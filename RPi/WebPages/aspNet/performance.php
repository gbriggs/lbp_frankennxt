<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>frankenNXT</title>
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="style.css" type="text/css" />
    <!--[if IE 6]>
    <link rel="stylesheet" href="fix.css" type="text/css" />
    <![endif]-->
  </head>

  <body>
     <div id="sidebar">
        <div id="logo">  
                <img src="./Images/frankenNXT-logo.png" alt="franenNXT"  />
            </a>
        </div>
        <h2>frankenNXT</h2>

        <div id="menu">
            <a  href="index.html">Admin</a>
            <a href="./WiFi.aspx">Wi-Fi Settings</a>
            <a class="active" href="performance.php">Processes</a>
            <a href="sysinfo.php">Compilers</a>
            <a href="piinfo.php">System</a>
            <a href="admin.php">Reboot / Shutdown</a>
        </div>
    </div>

    <div id="content">
      <h1>frankenNXT</h1>
     
      
        

     <meta http-equiv="refresh" content="5">   
        
<?php 
// format the uptime in case the browser doesn't support dhtml/javascript
// static uptime string
function format_uptime($seconds) {
  $secs = intval($seconds % 60);
  $mins = intval($seconds / 60 % 60);
  $hours = intval($seconds / 3600 % 24);
  $days = intval($seconds / 86400);

 $uptimeString = "";
  
  if ($days > 0) {
    $uptimeString .= $days;
    $uptimeString .= (($days == 1) ? " day" : " days");
  }
  if ($hours > 0) {
    $uptimeString .= (($days > 0) ? ", " : "") . $hours;
    $uptimeString .= (($hours == 1) ? " hour" : " hours");
  }
  if ($mins > 0) {
    $uptimeString .= (($days > 0 || $hours > 0) ? ", " : "") . $mins;
    $uptimeString .= (($mins == 1) ? " minute" : " minutes");
  }
  if ($secs > 0) {
    $uptimeString .= (($days > 0 || $hours > 0 || $mins > 0) ? ", " : "") . $secs;
    $uptimeString .= (($secs == 1) ? " second" : " seconds");
  }
  return $uptimeString;
}

// read in the uptime (using exec)
$uptime = exec("cat /proc/uptime");
$uptime = split(" ",$uptime);
$uptimeSecs = $uptime[0];

// get the static uptime
$staticUptime = "Server Uptime: ".format_uptime($uptimeSecs);
?>
        

        
        <div class="content">


<h3>Top Processes</h3>
<?php
$output = shell_exec('top -b -n 1 | head -n 15');
echo "<pre>$output</pre>";
?>



<h3>Uptime</h3>

<head>
<script language="javascript">
<!--
var upSeconds=<?php echo $uptimeSecs; ?>;
function doUptime() {
var uptimeString = "";
var secs = parseInt(upSeconds % 60);
var mins = parseInt(upSeconds / 60 % 60);
var hours = parseInt(upSeconds / 3600 % 24);
var days = parseInt(upSeconds / 86400);
if (days > 0) {
  uptimeString += days;
  uptimeString += ((days == 1) ? " day" : " days");
}
if (hours > 0) {
  uptimeString += ((days > 0) ? ", " : "") + hours;
  uptimeString += ((hours == 1) ? " hour" : " hours");
}
if (mins > 0) {
  uptimeString += ((days > 0 || hours > 0) ? ", " : "") + mins;
  uptimeString += ((mins == 1) ? " minute" : " minutes");
}
if (secs > 0) {
  uptimeString += ((days > 0 || hours > 0 || mins > 0) ? ", " : "") + secs;
  uptimeString += ((secs == 1) ? " second" : " seconds");
}
var span_el = document.getElementById("uptime");
var replaceWith = document.createTextNode(uptimeString);
span_el.replaceChild(replaceWith, span_el.childNodes[0]);
upSeconds++;
    
    
    
    
setTimeout("doUptime()",1000);
}
//-->
</script>
</head>
<body onLoad="doUptime();">


<!-- Uses the DIV tag, but SPAN can be used as well -->
<div id="uptime"><?php echo $staticUptime; ?></div>
</div>
</div>
</body>
</html>

