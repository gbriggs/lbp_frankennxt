﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;


namespace WiFiCtrl
{
    public partial class Settings : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //  get the address for this server 
            string localIP = "";
#if DEBUG
            //  debug is on remote machine, check settings
            localIP = ConfigurationManager.AppSettings["ipAddress"];
#else
            //  release is on the raspberry pi, but we don't know what mode we are in
            //  get the ip address of the local network
            localIP = Networking.GetFirstLocalIp();
#endif

            if (!Page.IsPostBack)
            {
                //  get the visible access points
                var response = Tcpip.SendString("$LBP_GET_APS", localIP, 48888);

                //  parse out the response
                Parser parseResponse = new Parser(response, ",");
                var command = parseResponse.GetNextString();
                var result = parseResponse.GetNextString();
                var message = parseResponse.GetNextString();

                string accessPoints = "";

                //  check the result
                if ( result == "ACK" )
                {
                    //  parse the access points
                    if (message.Length > 0)
                    {
                        accessPoints = message;

                        Parser parseAps = new Parser(accessPoints, "\n");

                        string nextAp = parseAps.GetNextString();
                        while (nextAp.Length > 0 && nextAp != "\n")
                        {
                            DropDownListAccessPoints.Items.Add(nextAp);
                            nextAp = parseAps.GetNextString();
                        }
                    }
                }
                else if ( message == "busy" )
                {
                    //  setup UI for busy state
                    SetUiForMode("busy");
                    //  post back here
                    Response.Redirect(Request.RawUrl);
                    return;
                }

                

                response = Tcpip.SendString("$LBP_GET_MODE", localIP, 48888);

                //  parse out the response
                parseResponse = new Parser(response, ",");
                command = parseResponse.GetNextString();
                result = parseResponse.GetNextString();
                message = parseResponse.GetNextString();

                if ( result == "ACK" )
                {
                    string mode = message;
                    if (accessPoints.Length < 1 || mode.Length < 1)
                    {
                        //  we failed to connect to the server
                        LabelSelectMode.Text = "Failed to query router mode, server did not respond!";
                        RadioButtonListMode.Visible = false;
                        LabelAccessPoints.Visible = false;
                        DropDownListAccessPoints.Visible = false;
                        LabelPassword.Visible = false;
                        TextBoxPassword.Visible = false;
                        ButtonSet.Visible = false;
                    }
                    else
                    {
                        //  set the initial mode
                        switch (mode)
                        {
                            case "Managed":
                            case "Master":
                                ViewState["initialMode"] = mode;
                                break;

                            default:
                                ViewState["initialMode"] = "invalid";
                                break;
                        }

                        SetUiForMode(mode);
                    }
                }
                else if ( message == "busy" )
                {
                    //  setup UI for busy state
                    SetUiForMode("busy");
                    //  post back here
                    Response.Redirect(Request.RawUrl);
                    return;
                }
            }
        }



        protected void SetUiForMode(string mode)
        {
            switch (mode)
            {
                case "Managed":
                    LabelSelectMode.Text = "Select WiFi mode:";
                    RadioButtonListMode.SelectedValue = mode;
                    LabelAccessPoints.Visible = true;
                    DropDownListAccessPoints.Visible = true;
                    LabelPassword.Visible = true;
                    TextBoxPassword.Visible = true;
                    ButtonSet.Visible = true;
                    ButtonSet.Text = "Connect to WiFi";
                    break;

                case "Master":
                    LabelSelectMode.Text = "Select WiFi mode:";
                    RadioButtonListMode.SelectedValue = mode;
                    LabelAccessPoints.Visible = false;
                    DropDownListAccessPoints.Visible = false;
                    LabelPassword.Visible = false;
                    TextBoxPassword.Visible = false;
                    ButtonSet.Text = "Set as Hotspot";

                    if (ViewState["initialMode"].ToString() == "Managed")
                        ButtonSet.Visible = true;
                    else
                        ButtonSet.Visible = false;

                    break;

                case "busy":
                    LabelSelectMode.Text = "Busy changing WiFi settings, checking again ...";
                    RadioButtonListMode.SelectedValue = "Master";
                    LabelAccessPoints.Visible = false;
                    DropDownListAccessPoints.Visible = false;
                    LabelPassword.Visible = false;
                    TextBoxPassword.Visible = false;
                    ButtonSet.Visible = false;
                    ButtonSet.Text = "";
                    break;
            }
        }


        protected void RadioButtonListMode_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetUiForMode(RadioButtonListMode.SelectedValue.ToString());
        }

        protected void ButtonSet_Click(object sender, EventArgs e)
        {
            switch (RadioButtonListMode.SelectedValue)
            {
                case "Master":
                    {
                        Response.Redirect("~/SetMode.aspx?mode=Master");
                    }
                    break;

                case "Managed":
                    {
                        Response.Redirect("~/SetMode.aspx?mode=Managed&ap=" + DropDownListAccessPoints.SelectedValue.ToString() + "&pw=" + TextBoxPassword.Text.ToString());
                    }
                    break;

                default:
                    break;
            }
        }

    }
}