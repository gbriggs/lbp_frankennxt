﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net.Sockets;
using System.Net;
using System.Net.NetworkInformation;

namespace WiFiCtrl
{
    public static class Tcpip
    {
        public const int SocketBufferSize = 5120;

        static public string SendString(string message, string ipAddress, int port, int timeout)
        {
            string response = "";

            try
            {
                // Establish the remote endpoint for the socket.
                IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse(ipAddress), port);

                // Create a TCP/IP  socket.
                Socket sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                sender.ReceiveTimeout = 3000;

                // Connect the socket to the remote endpoint. Catch any errors.
                try
                {
                    sender.Connect(remoteEP);

                    // Encode the data string into a byte array.
                    System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();

                    byte[] msg = encoding.GetBytes(message);

                    // Send the data through the socket.
                    int bytesSent = sender.Send(msg);

                    // Receive the response from the remote device.
                    // Data buffer for incoming data.
                    byte[] bytes = new byte[SocketBufferSize];

                    //  read from the sender
                    int bytesRec = sender.Receive(bytes);
                    response += Encoding.ASCII.GetString(bytes, 0, bytesRec);

                    //  make sure we got it all
                    while (bytesRec == SocketBufferSize)
                    {
                        bytesRec = sender.Receive(bytes);
                        response += Encoding.ASCII.GetString(bytes, 0, bytesRec);
                    }

                    // Release the socket
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();

                }
                catch (ArgumentNullException ane)
                {
                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se.ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e.ToString());
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }

            return response.TrimEnd('\0');
        }
        static public string SendString(string message, string ipAddress, int port)
        {
            return SendString(message, ipAddress, port, 5000);
        }


        static public bool CheckAvailableServerPort(int port)
        {
            bool isAvailable = true;

            // Evaluate current system tcp connections. This is the same information provided
            // by the netstat command line application, just in .Net strongly-typed object
            // form.  We will look through the list, and if our port we would like to use
            // in our TcpClient is occupied, we will set isAvailable to false.
            IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] tcpConnInfoArray = ipGlobalProperties.GetActiveTcpListeners();

            foreach (IPEndPoint endpoint in tcpConnInfoArray)
            {
                if (endpoint.Port == port)
                {
                    isAvailable = false;
                    break;
                }
            }

            return isAvailable;
        }

        static public bool IsValidV4IpAddress(string address)
        {
            IPAddress ipAddress;
            if (IPAddress.TryParse(address, out ipAddress))
            {
                switch (ipAddress.AddressFamily)
                {
                    case System.Net.Sockets.AddressFamily.InterNetwork:
                        // we have IPv4
                        return true;
                    case System.Net.Sockets.AddressFamily.InterNetworkV6:
                        return false;
                    default:
                        return false;
                }
            }
            return false;
        }

    }
}
