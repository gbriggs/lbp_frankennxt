﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;

namespace WiFiCtrl
{
    

    public partial class Default : System.Web.UI.Page
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
           //  get the address for this server 
            string localIP = "";
#if DEBUG
            //  debug is on remote machine, check settings
            localIP = ConfigurationManager.AppSettings["ipAddress"];
#else
            //  release is on the raspberry pi, but we don't know what mode we are in
            //  get the ip address of the local network
            localIP = Networking.GetFirstLocalIp();
#endif
            
          
            //  ping the monitor app for status
            var response = Tcpip.SendString( "$LBP_GET_STATUS", localIP , 48888);

            if ( response.Length == 0 )
            {
                //  no connection to the server, display error
                LabelStatus.Text = "Failed to get status, the server is not responding.";
                return;
            }

            //  parse out the response
            Parser parseResponse = new Parser(response, ",");
            var command = parseResponse.GetNextString();
            var result = parseResponse.GetNextString();
            var message = parseResponse.GetNextString();

            //  check response
            if ( result == "ACK" )
            {
                //  format the response for html, replace newline with breaks
                LabelStatus.Text = message.Replace("\n", "<br>");
            }
            else if ( result == "NAK" && message == "busy")
            {
                //  set the label and post back
                LabelStatus.Text = "Busy changing WiFi settings, checking again ...";
                //  post back here
                 Response.Redirect(Request.RawUrl);
            }
            else
            {
                LabelStatus.Text = "Error parsing response.";
            }
        }
    }
}