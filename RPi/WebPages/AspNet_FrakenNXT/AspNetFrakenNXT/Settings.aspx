﻿<%@ Page Title="WiFi Control" Language="C#" MasterPageFile="Site.Master" AutoEventWireup="true"
    CodeBehind="Settings.aspx.cs" Inherits="WiFiCtrl.Settings" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">

    

    <h1>Settings</h1>
   
    <p>
    <asp:Label ID="LabelSelectMode" runat="server" Text="Select WiFi mode:"></asp:Label>
    </p>
        <p>
        <asp:RadioButtonList ID="RadioButtonListMode" runat="server" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonListMode_SelectedIndexChanged">
            <asp:ListItem Value="Master">Use as a WiFi hotspot</asp:ListItem>
            <asp:ListItem Value="Managed">Connect to a WiFi network</asp:ListItem>
        </asp:RadioButtonList></p>
    <p>
        &nbsp;</p>
    
    <asp:Label ID="LabelAccessPoints" runat="server" Text="Access Point: "></asp:Label><br />
    <asp:DropDownList ID="DropDownListAccessPoints" runat="server" ToolTip="Select an access point to connect to.">
        </asp:DropDownList>
    
    <p>
    <asp:Label ID="LabelPassword" runat="server" Text="Password"></asp:Label>
    </p>
    <p>
        <asp:TextBox ID="TextBoxPassword" runat="server"></asp:TextBox>
    </p>
    <p>
        <asp:Button ID="ButtonSet" runat="server" Text="Set Mode" OnClick="ButtonSet_Click" />
    </p>
   
       
</asp:Content>