﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WiFiCtrl
{
    public class Parser
    {
        private string OriginalString = "";
        private string Buffer = "";
        private string Delimiter = "";

        public Parser(String originalString, String delimiter)
        {
            OriginalString = originalString;
            Buffer = OriginalString;
            Delimiter = delimiter;
        }

        public String GetNextString()
        {
            String returnString = "";
            int index = Buffer.IndexOf(Delimiter);

            if (index < 1)
            {
                returnString = Buffer;
                Buffer = "";
                return returnString;
            }

            returnString = Buffer.Substring(0, index);
            Buffer = Buffer.Substring(index + 1);
            return returnString;
        }

        public String GetRemainingBuffer()
        {
            return Buffer;
        }
    }
}
