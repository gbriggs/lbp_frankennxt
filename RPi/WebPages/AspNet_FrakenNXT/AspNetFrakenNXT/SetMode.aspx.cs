﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Net;

namespace WiFiCtrl
{
    public partial class SetMode : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //  get the address for this server 
            string localIP = "";
#if DEBUG
            //  debug is on remote machine, check settings
            localIP = ConfigurationManager.AppSettings["ipAddress"];
#else
            //  release is on the raspberry pi, but we don't know what mode we are in
            //  get the ip address of the local network
            localIP = Networking.GetFirstLocalIp();
#endif


            //  get mode to set
            var mode = Request.QueryString["mode"];
            switch (mode)
            {
                case "Master":
                    {
                        LabelSetModeStatus.Text = "Setting Raspberry Pi to hotspot mode, in a few moments you can find this device as access point \"RPi\".";

                        //  set router to master mode
                        var response = Tcpip.SendString("$LBP_SET_MODE,Master", localIP , 48888, 10);

                        
                    }
                    break;

                case "Managed":
                    {
                        //  set to managed mode
                        var ap = Request.QueryString["ap"];
                        var pw = Request.QueryString["pw"];

                        //
                  
                        LabelSetModeStatus.Text = "Attempting to connect to " + ap + ", in a few moments you can find this device on that network.";

                        var response = Tcpip.SendString("$LBP_SET_AP," + ap + "," + pw, localIP, 48888, 10);

                    }
                    break;

                default:
                    Response.Redirect("~/Default.aspx");
                    break;
            }

            

        }

        
    }
}